<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DatabasesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DatabasesTable Test Case
 */
class DatabasesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DatabasesTable
     */
    protected $Databases;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Databases',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Databases') ? [] : ['className' => DatabasesTable::class];
        $this->Databases = $this->getTableLocator()->get('Databases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Databases);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\DatabasesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\DatabasesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
