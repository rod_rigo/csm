<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChartersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChartersTable Test Case
 */
class ChartersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChartersTable
     */
    protected $Charters;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Charters',
        'app.Users',
        'app.Offices',
        'app.Feedbacks',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Charters') ? [] : ['className' => ChartersTable::class];
        $this->Charters = $this->getTableLocator()->get('Charters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Charters);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ChartersTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ChartersTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
