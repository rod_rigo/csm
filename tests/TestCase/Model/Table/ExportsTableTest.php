<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExportsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExportsTable Test Case
 */
class ExportsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ExportsTable
     */
    protected $Exports;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Exports',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Exports') ? [] : ['className' => ExportsTable::class];
        $this->Exports = $this->getTableLocator()->get('Exports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Exports);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ExportsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ExportsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
