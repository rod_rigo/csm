<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BackgroundsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BackgroundsTable Test Case
 */
class BackgroundsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BackgroundsTable
     */
    protected $Backgrounds;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Backgrounds',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Backgrounds') ? [] : ['className' => BackgroundsTable::class];
        $this->Backgrounds = $this->getTableLocator()->get('Backgrounds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Backgrounds);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\BackgroundsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\BackgroundsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
