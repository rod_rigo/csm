<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogosTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogosTable Test Case
 */
class LogosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LogosTable
     */
    protected $Logos;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Logos',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Logos') ? [] : ['className' => LogosTable::class];
        $this->Logos = $this->getTableLocator()->get('Logos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Logos);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\LogosTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\LogosTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
