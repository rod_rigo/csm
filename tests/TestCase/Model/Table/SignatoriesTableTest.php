<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SignatoriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SignatoriesTable Test Case
 */
class SignatoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SignatoriesTable
     */
    protected $Signatories;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Signatories',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Signatories') ? [] : ['className' => SignatoriesTable::class];
        $this->Signatories = $this->getTableLocator()->get('Signatories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Signatories);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SignatoriesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SignatoriesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
