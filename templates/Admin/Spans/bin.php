<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Span[]|\Cake\Collection\CollectionInterface $spans
 */
;?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Span</th>
                        <th>Ages</th>
                        <th>Modified By</th>
                        <th>Deleted</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<?=$this->Html->script('admin/spans/bin');?>

