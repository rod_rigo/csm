<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Profile Form</h3>
            </div>
            <?= $this->Form->create($user,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="form-group row">
                    <?=$this->Form->label('name', ucwords('Name'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('name',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Name'),
                            'required' => true,
                            'id' => 'name',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('username', ucwords('username'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('username',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('username'),
                            'required' => true,
                            'id' => 'username',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('email', ucwords('Email'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->email('email',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Email'),
                            'required' => true,
                            'id' => 'email',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('password', ucwords('Password'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->password('password',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Password'),
                            'required' => true,
                            'id' => 'password',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('confirm_password', ucwords('Password (Confirm)'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->password('confirm_password',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Password (Confirm)'),
                            'required' => true,
                            'id' => 'confirm-password',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('admin', '',[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('admin',[
                                'id' => 'admin',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true
                            ]);?>
                            <?=$this->Form->label('admin', ucwords('admin'));?>
                        </div>
                        <small></small>
                    </div>
                </div>
                <div class="form-group row">
                    <?=$this->Form->label('division', '',[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('division',[
                                'id' => 'division',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true
                            ]);?>
                            <?=$this->Form->label('division', ucwords('division'));?>
                        </div>
                        <small></small>
                    </div>
                </div>
                <div class="form-group row">
                    <?=$this->Form->label('office', '',[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('office',[
                                'id' => 'office',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true
                            ]);?>
                            <?=$this->Form->label('office', ucwords('office'));?>
                        </div>
                        <small></small>
                    </div>
                </div>
                <div class="form-group row">
                    <?=$this->Form->label('profile.office_id', ucwords('Office'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?=$this->Form->select('profile.office_id', $offices,[
                            'class' => 'form-control',
                            'id' => 'profile-office-id',
                            'required' => true,
                            'empty' => ucwords('select office'),
                        ]);?>
                        <small></small>
                    </div>
                </div>
                <div class="form-group row">
                    <?=$this->Form->label('profile.department_id', ucwords('Department'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?=$this->Form->select('profile.department_id', [],[
                            'class' => 'form-control',
                            'id' => 'profile-department-id',
                            'required' => true,
                            'empty' => ucwords('choose office'),
                        ]);?>
                        <small></small>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('is_admin',[
                    'id' => 'is-admin',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('is_division',[
                    'id' => 'is-division',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('is_office',[
                    'id' => 'is-office',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('token',[
                    'id' => 'token',
                    'value' => uniqid()
                ]);?>
                <?=$this->Form->hidden('profile.logical_id',[
                    'id' => 'profile-logical-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ]);?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Profiles', 'action' => 'index']);?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ]);?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/profiles/add');?>

