<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Logo|\Cake\Collection\CollectionInterface $logo
 */

$content = '';
$path = WWW_ROOT. 'img'. DS. $logo->logo;
$folder = new \Cake\Filesystem\File($path);
if($folder->exists()){
    $content = file_get_contents($path);
}

;?>

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                            <img src="#" id="image-preview" alt="" height="400" width="400">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-end align-items-center">
                    <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Logo Form</h3>
                </div>
                <?= $this->Form->create($logo,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ;?>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('title', ucwords('title'));?>
                            <?=$this->Form->text('title',[
                                'class' => 'form-control',
                                'id' => 'title',
                                'required' => true,
                                'placeholder' => ucwords('title'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center mt-2">
                            <img src="<?=$this->Url->assetUrl('/img/'.($logo->logo));?>" class="img-thumbnail" id="cropper" alt="Logo Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 d-flex justify-content-start align-items-center mt-2">
                            <button type="button" id="crop" class="btn btn-primary rounded-0" title="Crop">
                                <i class="fa fa-crop"></i>
                            </button>
                            <button type="button" id="move" class="btn btn-primary rounded-0 mr-2" title="Move">
                                <i class="fa fa-arrows-alt"></i>
                            </button>

                            <button type="button" id="zoom-in" class="btn btn-primary rounded-0" title="Zoom In">
                                <i class="fa fa-search-plus"></i>
                            </button>
                            <button type="button" id="zoom-out" class="btn btn-primary rounded-0 mr-2" title="Zoom Out">
                                <i class="fa fa-search-minus"></i>
                            </button>

                            <button type="button" id="rotate-left" class="btn btn-primary rounded-0" title="Rotate Left">
                                <i class="fa fa-undo"></i>
                            </button>
                            <button type="button" id="rotate-right" class="btn btn-primary rounded-0 mr-2" title="Rotate Right">
                                <i class="fa fa-redo"></i>
                            </button>

                            <button type="button" id="flip-vertical" class="btn btn-primary rounded-0" title="Flip Vertical">
                                <i class="fa fa-arrow-up"></i>
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <button type="button" id="flip-horizontal" class="btn btn-primary rounded-0 mr-2" title="Flip Horizontal">
                                <i class="fa fa-arrow-left"></i>
                                <i class="fa fa-arrow-right"></i>
                            </button>

                            <button type="button" id="move-up" class="btn btn-primary rounded-0" title="Move Up">
                                <i class="fa fa-arrow-circle-up"></i>

                            </button>
                            <button type="button" id="move-down" class="btn btn-primary rounded-0" title="Move Down">
                                <i class="fa fa-arrow-circle-down"></i>
                            </button>
                            <button type="button" id="move-left" class="btn btn-primary rounded-0" title="Move Left">
                                <i class="fa fa-arrow-circle-left"></i>

                            </button>
                            <button type="button" id="move-right" class="btn btn-primary rounded-0 mr-2" title="Move Down">
                                <i class="fa fa-arrow-circle-right"></i>
                            </button>

                            <button type="button" id="complete" class="btn btn-success rounded-0 pull-right" title="Complete">
                                <i class="fa fa-check"></i>
                            </button>
                        </div>

                        <div class="col-sm-12 col-md-2 col-lg-2">
                            <?=$this->Form->label('height', ucwords('height'));?>
                            <?=$this->Form->number('height',[
                                'class' => 'form-control',
                                'id' => 'height',
                                'required' => true,
                                'placeholder' => ucwords('height'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field'),
                                'min' => intval(1080),
                                'value' => intval(4096)
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-2 col-lg-2">
                            <?=$this->Form->label('width', ucwords('width'));?>
                            <?=$this->Form->number('width',[
                                'class' => 'form-control',
                                'id' => 'width',
                                'required' => true,
                                'placeholder' => ucwords('width'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field'),
                                'min' => intval(1080),
                                'value' => intval(4096)
                            ]);?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">

                            <div class="form-group">
                                <?=$this->Form->label('file', ucwords('file'));?>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <?=$this->Form->file('file',[
                                            'class' => 'custom-file-input rounded-0',
                                            'id' => 'header-file',
                                            'accept' => 'image/*',
                                        ]);?>
                                        <?=$this->Form->label('file', ucwords('Choose file'),[
                                            'class' => 'custom-file-label rounded-0'
                                        ]);?>
                                    </div>
                                </div>
                                <small></small>
                            </div>

                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="icheck-primary d-inline">
                                <?=$this->Form->checkbox('active',[
                                    'id' => 'active',
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => true,
                                ]);?>
                                <?=$this->Form->label('active', ucwords('Active'));?>
                            </div>
                            <small></small>
                        </div>

                    </div>

                </div>

                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?= $this->Form->hidden('user_id',[
                        'id' => 'user-id',
                        'value' => intval(@$auth['id'])
                    ]);;?>
                    <?= $this->Form->hidden('is_active',[
                        'id' => 'is-active',
                        'value' => intval(1),
                        'required' => true
                    ]);;?>
                    <?= $this->Form->hidden('logo',[
                        'id' => 'logo',
                        'value' => 'data:image/jpeg;base64,'.base64_encode($content),
                        'required' => true
                    ]);;?>

                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logos', 'action' => 'index']);?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                        Return
                    </a>
                    <?= $this->Form->button(__('Submit'),[
                        'class' => 'btn btn-success rounded-0',
                        'title' => ucwords('Submit')
                    ]);?>
                </div>
                <?= $this->Form->end() ;?>
            </div>
        </div>
    </div>

<?=$this->Html->script('admin/logos/edit');?>
