<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Patch[]|\Cake\Collection\CollectionInterface $patches
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Patches', 'action' => 'add']);?>" title="New Patch" class="btn btn-primary rounded-0">
            New Patch
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Patch</th>
                                    <th>Started At</th>
                                    <th>Modified By</th>
                                    <th>Modified</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/patches/index');?>
