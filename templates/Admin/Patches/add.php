<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Patch $patch
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<style>
    #patch {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
</style>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Patch Form</h3>
            </div>
            <?= $this->Form->create($patch,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('started_at', ucwords('started at'));?>
                        <?= $this->Form->date('started_at',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('title'),
                            'id' => 'started-at',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <?=$this->Form->label('patch', ucwords('patch'));?>
                        <?= $this->Form->textarea('patch',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('question'),
                            'id' => 'patch',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => intval(@$auth['id'])
                ]);?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Patches', 'action' => 'index']);?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ]);?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/patches/add');?>
