<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    var questions = <?=json_encode($questions);?>;
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card">
            <div class="card-body">
                <?=$this->Form->create(null,['class' => 'row mb-3', 'id' => 'form', 'type' => 'file']);?>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <?=$this->Form->label('start_date', ucwords('Start Date'));?>
                    <?=$this->Form->date('start_date',[
                        'id' => 'start-date',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('Start Date'),
                        'required' => true,
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <?=$this->Form->label('end_date', ucwords('End Date'));?>
                    <?=$this->Form->date('end_date',[
                        'id' => 'end-date',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'),
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('End Date'),
                        'required' => true
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2">
                    <?=$this->Form->label('records', ucwords('Records'));?>
                    <?=$this->Form->number('records',[
                        'id' => 'records',
                        'value' => 10000,
                        'class' => 'form-control form-control-border',
                        'title' => ucwords('Records'),
                        'min' => 10000,
                        'max' => 50000,
                        'required' => true
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-end">
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-primary rounded-0',
                        'type' => 'submit'
                    ]);?>
                </div>
                <?=$this->Form->end();?>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <div id="spreadsheet">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/advances/questions');?>
