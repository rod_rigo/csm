<?php
/**
 * @var \App\View\AppView $this
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$column = 'A';

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

foreach ($questions as $key => $question){

    $column = 'A';
    $count = 0;
    $questionId = intval($question->id);

    $option = (new \Cake\Collection\Collection($options))
        ->match(['question_id' => intval($question->id)])
        ->first();

    $column = ord(strval($column));
    $column += intval($option['total']);
    $letter = strval(chr($column));

    $title = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, strtoupper($question->title));
    $spreadsheet->addSheet($title, $key);

    $sheet = $spreadsheet->setActiveSheetIndex($key);

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), strtoupper($question->title))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setSize(18)
        ->setBold(true);

    $sheet
        ->mergeCells('A'.(strval($count)).':'.(strval($letter)).(strval($count)))
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), ucwords(strip_tags($question->question)))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setSize(12)
        ->setBold(true);

    $sheet
        ->mergeCells('A'.(strval($count)).':'.(strval($letter)).(strval($count)))
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), ucwords('Office'))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setSize(12)
        ->setBold(true);
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('92d050');
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);

    $column = 'A';
    $spreadsheet->getActiveSheet($key)
        ->getColumnDimension(strval($column))
        ->setWidth(150, 'pt');

    foreach ($question->options as $option){
        $column++;

        $sheet
            ->setCellValue(strval($column).(strval($count)), ucwords($option->choice->choice))
            ->getStyle(strval($column).(strval($count)))
            ->getFont()
            ->setSize(12)
            ->setBold(true);

        $sheet
            ->getStyle(strval($column).(strval($count)))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('92d050');

        $sheet
            ->getStyle(strval($column).(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
            ->setWrapText(true);

        $spreadsheet->getActiveSheet($key)
            ->getColumnDimension(strval($column))
            ->setWidth(150, 'pt');
    }

    $startColumn = $count;
    $startColumn++;

    foreach ($offices as $key => $office){

        $count++;
        $header = 'A';
        $officeId = intval($office->id);

        $sheet
            ->setCellValue('A'.(strval($count)), ucwords($office->office))
            ->getStyle('A'.(strval($count)))
            ->getFont()
            ->setSize(12)
            ->setBold(true);

        $sheet
            ->getStyle(strval($column).(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
            ->setWrapText(true);

        foreach ($question->options as $option){

            $header++;
            $choiceId = intval($option->choice_id);

            $answer = (new \Cake\Collection\Collection($answers))
                ->match([
                    'question_id' => intval($questionId),
                    'office_id' => intval($officeId),
                    'choice_id' => intval($choiceId)
                ])
                ->sumOf('total');

            if(!empty($answer)){
                $sheet
                    ->setCellValue(strval($header).(strval($count)), doubleval($answer))
                    ->getStyle(strval($header).(strval($count)))
                    ->getFont()
                    ->setSize(12)
                    ->setBold(true);

                $sheet
                    ->getStyle(strval($header).(strval($count)))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
                    ->setWrapText(true);
            }else{
                $sheet
                    ->setCellValue(strval($header).(strval($count)), doubleval(0))
                    ->getStyle(strval($header).(strval($count)))
                    ->getFont()
                    ->setSize(12)
                    ->setBold(true);

                $sheet
                    ->getStyle(strval($header).(strval($count)))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
                    ->setWrapText(true);
            }

        }

    }

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), ucwords('Total'))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setSize(12)
        ->setBold(true);
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('ffff00');
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);

    $sheet
        ->mergeCells('B'.(strval($count)).':'.(strval($letter)).(strval($count)))
        ->getStyle('B'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $sheet
        ->getStyle('B'.(strval($count)))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('5b9bd5');
    $sheet
        ->getStyle('B'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);
    $sheet
        ->getStyle('B'.(strval($count)))
        ->getFont()
        ->setSize(12)
        ->setBold(true);

    $sheet->setCellValue(
        strval('B').(strval($count)),
        '=SUM('.(strval('B').strval(intval($startColumn))).':'.(strval($letter).strval((intval($count)))).')'
    );

}

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')
    ->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->filename = ucwords($filename);
$export->mime = strtolower('excel');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)) {
// Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', ucwords($filename))) . '.xlsx') . '"');
    $writer->save('php://output');
    exit(0);
}