<?php
/**
 * @var \App\View\AppView $this
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$column = 'A';

//$styleArray = [
//    'font' => [
//        'bold' => true,
//    ],
//    'alignment' => [
//        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
//    ],
//    'borders' => [
//        'top' => [
//            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
//        ],
//    ],
//    'fill' => [
//        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
//        'rotation' => 90,
//        'startColor' => [
//            'argb' => 'FFA0A0A0',
//        ],
//        'endColor' => [
//            'argb' => 'FFFFFFFF',
//        ],
//    ],
//];
//$spreadsheet->getActiveSheet()->getStyle('B3:B7')->applyFromArray($styleArray);

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

foreach ($offices as $key => $office){

    $column = 'A';
    $count = 0;
    $officeId = intval($office->id);

    $column = ord(strval($column));
    $column += intval(count($office->departments));
    $letter = strval(chr($column));

    $title = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, strtoupper($office->legend));
    $spreadsheet->addSheet($title, $key);

    $sheet = $spreadsheet->setActiveSheetIndex($key);

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), strtoupper($office->legend))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setSize(18)
        ->setBold(true);

    $sheet
        ->mergeCells('A'.(strval($count)).':'.(strval($letter)).(strval($count)))
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    $column = 'A';
    $spreadsheet->getActiveSheet($key)
        ->getColumnDimension(strval($column))
        ->setWidth(160, 'pt');

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), ucwords('service quality dimensions'))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE ) )
        ->setSize(12)
        ->setBold(true);
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('045e55');
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);
    $sheet
        ->getStyle((strval('A')).(strval($count)))
        ->getBorders()
        ->getAllBorders()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
        ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK ) );

    foreach ($office->departments as $department){
        $column++;

        $sheet
            ->setCellValue((strval($column)).(strval($count)), strtoupper($department->department))
            ->getStyle((strval($column)).(strval($count)))
            ->getFont()
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE ) )
            ->setSize(12)
            ->setBold(true);
        $sheet
            ->getStyle((strval($column)).(strval($count)))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('045e55');
        $sheet
            ->getStyle((strval($column)).(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
            ->setWrapText(true);
        $sheet
            ->getStyle((strval($column)).(strval($count)))
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK ) );

        $spreadsheet->getActiveSheet($key)
            ->getColumnDimension(strval($column))
            ->setWidth(160, 'pt');
    }

    foreach ($questions as $question){
        $count++;

        $sheet->getRowDimension(strval($count))
            ->setRowHeight(25, 'pt');

        $sheet
            ->setCellValue('A'.(strval($count)), ucwords($question['sub_title']))
            ->getStyle('A'.(strval($count)))
            ->getFont()
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE ) )
            ->setSize(12)
            ->setBold(true);
        $sheet
            ->getStyle('A'.(strval($count)))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('045e55');
        $sheet
            ->getStyle('A'.(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
            ->setWrapText(true);
        $sheet
            ->getStyle((strval('A')).(strval($count)))
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK ) );

        $column = 'B';

    }

    $column = 'B';
    foreach ($office->departments as $department){

        $departmentId = intval($department->id);

        $header = 3;
        foreach ($questions as $question){

            $questionId = intval($question['id']);
            $isComputed = (new \Cake\Collection\Collection($question['options']))
                ->filter(function($value, $key){
                    return boolval($value['is_computed']);
                })->toArray();
            $isSubtracted = (new \Cake\Collection\Collection($question['options']))
                ->filter(function($value, $key){
                    return boolval($value['is_subtracted']);
                })->toArray();

            $computed = (new \Cake\Collection\Collection($answers))
                ->filter(function($value, $key) use($isComputed){
                    return in_array(intval($value['choice_id']), array_column($isComputed,'choice_id'));
                })->match([
                    'office_id' => intval($officeId),
                    'department_id' => intval($departmentId),
                    'question_id' => intval($questionId)
                ])->sumOf('total');

            $subtracted = (new \Cake\Collection\Collection($answers))
                ->filter(function($value, $key) use($isSubtracted){
                    return in_array(intval($value['choice_id']), array_column($isSubtracted,'choice_id'));
                })->match([
                    'office_id' => intval($officeId),
                    'department_id' => intval($departmentId),
                    'question_id' => intval($questionId)
                ])->sumOf('total');

            $total = (new \Cake\Collection\Collection($surveys))->match([
                'office_id' => intval($officeId),
                'department_id' => intval($departmentId),
            ])->sumOf('total');

            $computation = intval(0);

            if((doubleval($total) - doubleval($subtracted)) != 0){
                $computation = doubleval($computed) / (doubleval($total) - doubleval($subtracted));
                $computation = doubleval($computation) * doubleval(100);
            }

            $sheet
                ->setCellValue((strval($column)).(strval($header)), round(doubleval($computation),2))
                ->getStyle((strval($column)).(strval($header)))
                ->getFont()
                ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE ) )
                ->setSize(12)
                ->setBold(true);

            $sheet
                ->getStyle((strval($column)).(strval($header)))
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('0f9171');

            $sheet
                ->getStyle((strval($column)).(strval($header)))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
                ->setWrapText(true);
            $sheet
                ->getStyle((strval($column)).(strval($header)))
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK ) );

            $header++;
        }

        $column++;

    }

    $count++;
    $sheet
        ->setCellValue('A'.(strval($count)), strtoupper('Respondents'))
        ->getStyle('A'.(strval($count)))
        ->getFont()
        ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE ) )
        ->setSize(12)
        ->setBold(true);
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('045e55');
    $sheet
        ->getStyle('A'.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);
    $sheet
        ->getStyle((strval('A')).(strval($count)))
        ->getBorders()
        ->getAllBorders()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
        ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK ) );

    $column = 'B';
    foreach ($office->departments as $department){
        $departmentId = intval($department->id);

        $total = (new \Cake\Collection\Collection($surveys))->match([
            'office_id' => intval($officeId),
            'department_id' => intval($departmentId),
        ])->sumOf('total');

        $sheet
            ->setCellValue((strval($column)).(strval($count)), doubleval($total))
            ->getStyle((strval($column)).(strval($count)))
            ->getFont()
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE ) )
            ->setSize(12)
            ->setBold(true);

        $sheet
            ->getStyle((strval($column)).(strval($count)))
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('0f9171');

        $sheet
            ->getStyle((strval($column)).(strval($count)))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
            ->setWrapText(true);
        $sheet
            ->getStyle((strval($column)).(strval($count)))
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK ) );

        $column++;
    }

}

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')
    ->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->filename = ucwords($filename);
$export->mime = strtolower('excel');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)) {
// Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', ucwords($filename))) . '.xlsx') . '"');
    $writer->save('php://output');
    exit(0);
}