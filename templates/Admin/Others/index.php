<?php
/**
 * @var \App\View\AppView $this
 * Created by PhpStorm.
 * User: user
 * Date: 4/1/2024
 * Time: 9:12 AM
 */

$months = [
    ['text' => 'January', 'value' => 'January', 'selected' => (strtoupper('January') == strtoupper(date('F')))? true: false],
    ['text' => 'February', 'value' => 'February', 'selected' => (strtoupper('February') == strtoupper(date('F')))? true: false],
    ['text' => 'March', 'value' => 'March', 'selected' => (strtoupper('March') == strtoupper(date('F')))? true: false],
    ['text' => 'April', 'value' => 'April', 'selected' => (strtoupper('April') == strtoupper(date('F')))? true: false],
    ['text' => 'May', 'value' => 'May', 'selected' => (strtoupper('May') == strtoupper(date('F')))? true: false],
    ['text' => 'June', 'value' => 'June', 'selected' => (strtoupper('June') == strtoupper(date('F')))? true: false],
    ['text' => 'July', 'value' => 'July', 'selected' => (strtoupper('July') == strtoupper(date('F')))? true: false],
    ['text' => 'August', 'value' => 'August', 'selected' => (strtoupper('August') == strtoupper(date('F')))? true: false],
    ['text' => 'September', 'value' => 'September', 'selected' => (strtoupper('September') == strtoupper(date('F')))? true: false],
    ['text' => 'October', 'value' => 'October', 'selected' => (strtoupper('October') == strtoupper(date('F')))? true: false],
    ['text' => 'November', 'value' => 'November', 'selected' => (strtoupper('November') == strtoupper(date('F')))? true: false],
    ['text' => 'December', 'value' => 'December', 'selected' => (strtoupper('December') == strtoupper(date('F')))? true: false]
];

?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card">
            <div class="card-body">
                <?=$this->Form->create(null,['id' => 'form', 'class' => 'row mb-3', 'type' => 'file']);?>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('year', ucwords('Year'));?>
                        <?=$this->Form->year('year',[
                            'class' => 'form-control form-control-border rounded-0',
                            'id' => 'year',
                            'required' => true,
                            'title' => ucwords('select year'),
                            'empty' => false,
                            'min' => 2000,
                            'value' => date('Y')
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('month', ucwords('Month'));?>
                        <?=$this->Form->select('month', $months,[
                            'id' => 'month',
                            'class' => 'form-control form-control-border rounded-0',
                            'title' => ucwords('Month'),
                            'empty' => ucwords('All')
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('question', ucwords('Question (Except)'));?>
                        <?=$this->Form->select('question', $questions,[
                            'id' => 'question',
                            'class' => 'form-control form-control-border rounded-0',
                            'title' => ucwords('Question'),
                            'empty' => ucwords('Question'),
                            'multiple' => true,
                        ]);?>
                    </div>
                <?=$this->Form->end();?>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover text-nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Report</th>
                                        <th>Filename</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Question</td>
                                        <td>
                                            <input type="text" name="filename" value="Questions" class="form-control form-control-sm" id="filename-0">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-secondary rounded-0 export" data-id="0" data-url="questions">Export</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CSM Report</td>
                                        <td>
                                            <input type="text" name="filename" value="CSM_Report_Per_Office" class="form-control form-control-sm" id="filename-1">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-secondary rounded-0 export" data-id="1" data-url="csm">Export</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/others/index');?>
