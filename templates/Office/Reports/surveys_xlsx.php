<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$count = intval(0);
$column = 'A';

$spreadsheet = new Spreadsheet();
$worksheet = $spreadsheet->setActiveSheetIndex(intval(0));

$count++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('NO.'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('Reference code'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('DATE'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('Client type'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('sex'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('age'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('region of residence'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

    $charter = 1;
    $sdq = 1;
foreach ($questions as $question){
    $column++;
    $worksheet
        ->setCellValue($column.(strval($count)), strtoupper( boolval($question->is_charter)? 'CC'.$charter++: 'SQD'.$sdq++ ))
        ->getStyle($column.(strval($count)))
        ->getFont()
        ->setBold(true);
    $spreadsheet->getActiveSheet()
        ->getStyle($column.(strval($count)))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()
        ->getStyle($column.(strval($count)))
        ->getBorders()
        ->getBottom()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()
        ->getStyle($column.(strval($count)))
        ->getBorders()
        ->getLeft()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    foreach ($question->options as $key => $option){
        $column++;
        $worksheet
            ->setCellValue($column.(strval($count)), strtoupper( intval($key + 1)))
            ->getStyle($column.(strval($count)))
            ->getFont()
            ->setBold(true);
        $spreadsheet->getActiveSheet()
            ->getStyle($column.(strval($count)))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()
            ->getStyle($column.(strval($count)))
            ->getBorders()
            ->getBottom()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()
            ->getStyle($column.(strval($count)))
            ->getBorders()
            ->getLeft()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    }

}

$column++;
$worksheet
    ->setCellValue($column.(strval($count)), strtoupper('Comments/Suggestions'))
    ->getStyle($column.(strval($count)))
    ->getFont()
    ->setBold(true);
$worksheet
    ->getStyle($column.(strval($count)))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getBottom()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
$spreadsheet->getActiveSheet()
    ->getStyle($column.(strval($count)))
    ->getBorders()
    ->getLeft()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

$spreadsheet->getActiveSheet()->getStyle('A'.(1).':'.($column).''.($count).'')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('BEDDBA');

$header = 'A';
foreach ($surveys as $key => $survey){
    $count++;
    $worksheet
        ->setCellValue($header.(strval($count)), strval($survey->transaction_code));
    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), $survey->ca_no);
    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), date('Y-m-d', strtotime($survey->created)));
    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), strtoupper($survey->customer_type->customer_type));
    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), strtoupper($survey->visitor->gender->gender));
    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), strtoupper($survey->visitor->age));
    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), strtoupper($survey->visitor->agency));

    foreach ($questions as $k => $question){
        $header++;
        $worksheet
            ->setCellValue($header.(strval($count)), strip_tags($question->question))
            ->getStyle($header.(strval($count)))
            ->getAlignment()->setWrapText(false);

        foreach ($survey->answers as $answer){
            if($answer->question_id == $question->id){
                foreach ($question->options as $option){
                    $header++;
                    if(intval($answer->choice_id) == intval($option->choice_id)){
                        $worksheet
                            ->setCellValue($header.(strval($count)), 1);
                        $worksheet
                            ->getStyle($header.(strval($count)))
                            ->getAlignment()
                            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                    }else{
                        $worksheet
                            ->setCellValue($header.(strval($count)), null);
                    }

                }
            }
        }

    }

    $header++;
    $worksheet
        ->setCellValue($header.(strval($count)), strtoupper(strip_tags($survey->remarks)));

    $header = 'A';
}

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');


$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')
    ->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->filename = ucwords($filename);
$export->mime = strtolower('excel');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)){
    // Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="'. urlencode(''.ucwords(preg_replace('/\s+/', '_', $filename)).'.xlsx').'"');
    $writer->save('php://output');
    exit(0);
}

