<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question[]|\Cake\Collection\CollectionInterface $questions
 */

$months = [
    ['text' => 'January', 'value' => 'January'],
    ['text' => 'February', 'value' => 'February'],
    ['text' => 'March', 'value' => 'March'],
    ['text' => 'April', 'value' => 'April'],
    ['text' => 'May', 'value' => 'May'],
    ['text' => 'June', 'value' => 'June'],
    ['text' => 'July', 'value' => 'July'],
    ['text' => 'August', 'value' => 'August'],
    ['text' => 'September', 'value' => 'September'],
    ['text' => 'October', 'value' => 'October'],
    ['text' => 'November', 'value' => 'November'],
    ['text' => 'December', 'value' => 'December']
];

?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="modal fade" id="modal" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create(null,['class' => 'w-100', 'id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('year', ucwords('Year'));?>
                        <?=$this->Form->year('year',[
                            'id' => 'year',
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y'),
                            'class' => 'form-control form-control-border',
                            'title' => ucwords('Year'),
                            'required' => true,
                            'min' => 2000,
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('month', ucwords('Month'));?>
                        <?=$this->Form->select('month', $months,[
                            'id' => 'month',
                            'class' => 'form-control form-control-border',
                            'title' => ucwords('Month'),
                            'empty' => ucwords('Month')
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <?=$this->Form->label('records', ucwords('Records'));?>
                        <?=$this->Form->number('records',[
                            'id' => 'records',
                            'value' => 10000,
                            'class' => 'form-control form-control-border',
                            'title' => ucwords('Records'),
                            'min' => 10000,
                            'max' => 50000,
                            'required' => true
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                        <?=$this->Form->label('Services', ucwords('Services'));?>
                        <?=$this->Form->select('services', $services,[
                            'id' => 'services',
                            'class' => 'form-control form-control-border',
                            'empty' => ucwords('Services'),
                            'required' => true
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between align-items-center">
                <?=$this->Form->button(ucwords('XlSX'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'button',
                    'id' => 'xlsx',
                    'title' => ucwords('XlSX')
                ]);?>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-primary rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ]);?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="Configuration">
            Configuration
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>CA No</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>Agency/Office</th>
                                    <th>Purpose</th>
                                    <th>Customer Type</th>
                                    <th>Office</th>
                                    <th>Service</th>
                                    <th>Modified</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('office/reports/surveys');?>

