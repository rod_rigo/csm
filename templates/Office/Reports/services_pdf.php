<?php

use Dompdf\Dompdf;
use Dompdf\Options;
use Cake\Filesystem\File;

$filename = ($service->service).', '.($service->office->office).' '.($asOf);

$file = new File(WWW_ROOT . 'img' . DS . $banner->header);
if($file->exists()){
    $path = WWW_ROOT. 'img'. DS. $banner->header;
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $header = 'data:'.($mime).';base64,'.($image);
}else{
    $path = WWW_ROOT.'img'. DS.'header.jpg';
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $header = 'data:'.($mime).';base64,'.($image);
}
$headerPath = $path;

$file = new File(WWW_ROOT . 'img' . DS . $banner->footer);
if ($file->exists()) {
    $path = WWW_ROOT . 'img' . DS . $banner->footer;
} else {
    $path = WWW_ROOT . 'img' . DS . 'footer.png';
}
$footerPath = $path;


$options = new Options();
$options->set('isPhpEnabled', true);
$options->set('isRemoteEnabled', true);
$options->set('isHtml5ParserEnabled', true);

$content = '';

$signatoryNames = '';
$signatoryPositions = '';
foreach ($signatories as $signatory){
    $signatoryPositions .= ' <td style="text-align: center; padding: 0;border: none; ;border-collapse: collapse;">'.($signatory->position).'</td>';
    $signatoryNames .= '<th style="text-align: center; font-weight: bolder; border-collapse: collapse; padding: 4px;">'.($signatory->signatory).'</th>';
}

$content .= '<table style="border: none; margin-top: 10px; width: 100%; border-collapse: collapse;">';
$content .= '<thead>
<tr>
    <th style="width: 40%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">Office:</th>
    <th style="width: 60%; font-size: 16px; font-weight: normal; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">'.(ucwords($service->office->office)).'</th>
</tr>
<tr>
    <th style="width: 40%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px; text-align: left;">Department</th>
     <th style="width: 60%; font-size: 16px; font-weight: normal; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">'.(ucwords($service->department->department)).'</th>
</tr>
<tr>
    <th style="width: 40%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">Service</th>
   <th style="width: 60%; font-size: 16px; font-weight: normal; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">'.(ucwords($service->service)).'</th>
</tr>
<tr>
    <th style="width: 40%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">Total Number Of Transactions</th>
    <th style="width: 60%; font-size: 16px; font-weight: normal; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: left;">'.(doubleval($total)).'</th>
</tr>
</thead>';
$content .= '</table>';
$content .= '<br>';
$content .= '<br>';
$content .= '<table style="border: none; margin-top: 10px; width: 100%; border-collapse: collapse;">';
$content .= '<thead>
    <tr>
        <th style="width: 100%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: center;">Client Type</th>
        <th></th>
    </tr>
</thead>';
$content .= '<tbody>';
$customerTypesCollection = (new \Cake\Collection\Collection($customerTypes));
foreach ($customerType as $row){
    $content .= '<tr><td style="width: 70%; font-size: 16px; border-collapse: collapse; padding: 4px;text-align: left;">'.($row['customer_type']).'</td>
        <td style="width: 30%;font-size: 16px; border-collapse: collapse; padding: 4px;text-align: left;">'.(@intval($customerTypesCollection->match(['customer_type' => $row['customer_type']])->first()['total'])).'</td></tr>';
}
$content .= '</tbody>';
$content .= '</table>';

$content .= '<br>';
$content .= '<br>';
$content .= '<table style="border: none; margin-top: 10px; width: 100%; border-collapse: collapse;">';
$content .= '<thead style="border: none;">
    <tr>
        <th style="width: 100%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px; text-align: center;">Age</th>
        <th></th>
    </tr>
</thead>';
$content .= '<tbody style="border: none;">';
$spansCollection = (new \Cake\Collection\Collection($spans));
foreach ($span as $row){
    $content .= '<tr style="border: none;"><td style="width: 70%; font-size: 16px;border-collapse: collapse; padding: 4px; text-align: left;">'.($row['span']).'</td>
        <td style="width: 30%;font-size: 16px;border-collapse: collapse; padding: 4px; text-align: left;">'.(intval(@$spansCollection->match(['span' => $row['span']])->first()['total'])).'</td></tr>';
}
$content .= '</tbody>';
$content .= '</table>';

//
$content .= '<br>';
$content .= '<br>';
$content .= '<table style="border: none; margin-top: 10px; width: 100%; border-collapse: collapse;">';
$content .= '<thead>
    <tr>
        <th style="width: 100%; font-size: 16px; border: 1px none black;border-collapse: collapse; padding: 4px;text-align: center;">Gender</th>
        <th></th>
    </tr>
</thead>';
$content .= '<tbody>';
$genderCollection = (new \Cake\Collection\Collection($genders));
foreach ($gender as $row){
    $content .= '<tr><td style="width: 70%; font-size: 16px;border-collapse: collapse; padding: 4px;text-align: left;">'.($row['gender']).'</td>
        <td style="width: 30%;font-size: 16px;border-collapse: collapse; padding: 4px; text-align: left;">'.(intval(@$genderCollection->match(['gender' => $row['gender']])->first()['total'])).'</td></tr>';
}
$content .= '</tbody>';
$content .= '</table>';
$content .= '<br>';
$content .= '<br>';

$content .='<div style="page-break-after: always;"></div>';

$isCharter = [
    ucwords('Service Quality Dimension'),
    ucwords('Citizen’s Charter Awareness'),
];

$type = [
    strtoupper('SQD'),
    strtoupper('CC'),
];

$answer = (new \Cake\Collection\Collection($answers));
$colspan = (new \Cake\Collection\Collection($choices));

$content .= '<table style="border: 1px solid black; margin-top: 10px; width: 100%; font-size: 16px; border-collapse: collapse;">';
$content .= '<thead style="width: 100%;">
            <tr style="text-align: center;">
               <th style="text-align: center; border: 1px solid black;border-collapse: collapse; padding: 4px; width: 100%;" colspan="2">'.($isCharter[intval(1)]).'</th>';
$content .= '</tr>
            </thead>';
$content .= '<tbody>';
foreach ($charters as $key => $charter){
    $questionId = $charter['id'];
    $content .= '<tr>';
    $content .= '<td style="border: 1px solid black;border-collapse: collapse; padding: 4px; width: 100%; font-size: 16px; text-align: center; text-transform: uppercase;" colspan="2">'.(strtoupper($charter['sub_title'])).'</td>';
    $content .= '</tr>';

    foreach ($charter['options'] as $option){
        $content .= '<tr>';
        $content .= '<td style=" width: 70%; text-align: left; border: 1px solid black;border-collapse: collapse; font-size: 16px;">'.($option['position']).') '.(strval($option['choice']['choice'])).'</td>';
        $content .= '<td style="text-align: center; border: 1px solid black;border-collapse: collapse; padding: 4px; width: 30%; font-size: 16px;">'.(intval(@$answer->match(['question_id' => intval($questionId), 'choice_id' => intval($option['choice_id'])])->first()['total'])).'</td>';
        $content .= '</tr>';
    }

}
$content .= '</tbody>';
$content .= '</table>';

$content .='<div style="page-break-after: always;"></div>';

//
$content .= '<table style="border: 1px solid black; margin-top: 10px; width: 100%; font-size: 16px; border-collapse: collapse;">';
$content .= '<thead style="width: 100%;">
            <tr style="text-align: center;">
               <th style="text-align: center; border: 1px solid black;border-collapse: collapse; padding: 4px;" colspan="2">'.($isCharter[intval(0)]).'</th>';
$content .= '</tr>
            </thead>';
$content .= '<tbody>';

foreach ($sqds as $key => $sqd){
    $questionId = $sqd['id'];
    $content .= '<tr>';
    $content .= '<td style="border: 1px solid black;border-collapse: collapse; padding: 4px; text-align: center; text-transform: uppercase;" colspan="2">'.(strtoupper($sqd['sub_title'])).'</td>';
    $content .= '</tr>';

    foreach ($sqd['options'] as $option){
        $content .= '<tr>';
        $content .= '<td style="border: 1px solid black;border-collapse: collapse; padding: 4px; width: 70%; text-align: left;">'.($option['position']).')'.($option['choice']['choice']).'</td>';
        $content .= '<td style="text-align: center; border: 1px solid black;border-collapse: collapse; padding: 4px; width: 30%;">'.(intval(@$answer->match(['question_id' => intval($questionId), 'choice_id' => intval($option['choice_id'])])->first()['total'])).'</td>';
        $content .= '</tr>';
    }
}
$content .= '</tbody>';
$content .= '</table>';

$content .= '<p style="text-align: left; font-weight: bold; font-size: 14px; margin-top: 20px; margin-bottom: 20px;">Certified True and Correct:</p>';

$content .= '<table style="border: none; margin-top: 10px; width: 100%; font-size: 14px; border-collapse: collapse;">';
$content .= '<thead style="width: 100%;">
            <tr>';
$content .= $signatoryNames;
$content .= '</tr>
            </thead>';
$content .= '<tbody>';
$content .= '<tr>
    '.($signatoryPositions).'
</tr>';
$content .= '</tbody>';
$content .= '</table>';

// HTML content for the certificate
$html = '
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>'.($asOf).'</title>
</head>
<style>
    *{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }
    body{
        padding-bottom: 5em;
        padding-left: 3em;
        padding-right: 3em;
        padding-top: 2em;
    }
</style>
<body>
<img src="' . ($header) . '" style="width: 100%; height: 12em;">

<p style="text-align: center; font-weight: bold; font-size: 18px;">Client Satisfaction Measurement Result</p>
<p style="text-align: center; font-weight: bold; font-size: 18px;">As Of '.(ucwords($asOf)).'</p>

'.($content).'
</body>
</html>

';

// Instantiate Dompdf
$dompdf = new Dompdf($options);

// Load HTML content
$dompdf->loadHtml($html);

//$dompdf->setPaper([0, 0, 828/2, 585], 'landscape');
$dompdf->setPaper('A4');

// Render PDF (DOMPDF)
$dompdf->render();
$canvas = $dompdf->get_canvas();
$canvas->page_script(function ($pageNumber, $pageCount, $canvas, $fontMetrics) use ($footerPath){
    $width = $canvas->get_width();
    $height = $canvas->get_height();
//    $canvas->image($header, intval(0), intval(15), (intval($width) - intval(10)), intval(150));

//    Path/vertical/height/width/horizontal
    $canvas->image($footerPath, intval(0), (intval($height) - intval(95)), (intval($width) - intval(0)), intval(80));

//
    $canvas->text(intval(0), intval(0), '', $fontMetrics->get_font('Arial', 'normal'), 12);
    $canvas->text((intval($width) - intval(100)), (intval($height) - intval(17)), "Page $pageNumber of $pageCount", $fontMetrics->get_font('Arial', 'normal'), intval(10));
});

// Output PDF to browser
$dompdf->stream($filename.'.pdf', ['Attachment' => false]);

exit(0);
