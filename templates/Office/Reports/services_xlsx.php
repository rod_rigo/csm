<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

$count = 8;
$column = 'A';
$filename = $service->office->office.' '.$service->service;

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Office:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), ucwords($service->office->office))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('department:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), ucwords($service->department->department))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('service:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), ucwords($service->service))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Total Number of Transactions:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('B'.strval($count), doubleval($total))
    ->getStyle('B'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;
$count++;
$age = $count;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Client Type'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);
$customerTypeCollection = (new \Cake\Collection\Collection($customerTypes));
foreach ($customerType as $row){
    $count++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($count), ucwords($row['customer_type'].':'))
        ->getStyle('A'.strval($count))
        ->getFont()
        ->setBold(true);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('B'.strval($count), doubleval(@$customerTypeCollection->match(['customer_type' => $row['customer_type']])->first()['total']))
        ->getStyle('B'.strval($count))
        ->getFont()
        ->setBold(false);
}

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('C'.strval($age), ucwords('Age'))
    ->getStyle('C'.strval($age))
    ->getFont()
    ->setBold(true);

$spanCollection = (new \Cake\Collection\Collection($spans));
foreach ($span as $row){
    $age++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('C'.strval($age), ucwords($row['span'].':'))
        ->getStyle('C'.strval($age))
        ->getFont()
        ->setBold(true);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('D'.strval($age), doubleval(@$spanCollection->match(['span' => $row['span']])->first()['total']))
        ->getStyle('D'.strval($age))
        ->getFont()
        ->setBold(false);
}


$count++;
$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Sex'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(true);

$genderCollection = (new \Cake\Collection\Collection($genders));
foreach ($gender as $row){
    $count++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($count), ucwords($row['gender'].':'))
        ->getStyle('A'.strval($count))
        ->getFont()
        ->setBold(true);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('B'.strval($count), doubleval(@$genderCollection->match(['gender' => $row['gender']])->first()['total']))
        ->getStyle('B'.strval($count))
        ->getFont()
        ->setBold(false);
}

$isCharter = [
    ucwords('Service Quality Dimension'),
    ucwords('Citizen’s Charter Awareness'),
];

$type = [
    strtoupper('SQD'),
    strtoupper('CC'),
];

$choices = new \Cake\Collection\Collection($answers->toArray());

foreach ($collections->toList() as $key => $collection){
    $count++;
    $count++;;
    $checkCharter = $key;

    $start = 0;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.strval($count), ucwords($isCharter[intval($key)]))
        ->getStyle('A'.strval($count))
        ->getFont()
        ->setBold(true);

    foreach ($collection as $k => $v){

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.strval($count), ucwords($v['sub_title']))
            ->getStyle('A'.strval($count))
            ->getFont()
            ->setBold(true);
        $spreadsheet->setActiveSheetIndex(0)
            ->getStyle('A'.strval($count))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $count++;
        foreach ($v['options'] as $key => $option){
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue(strval('A').strval($count), $option['choice']['choice'])
                ->getStyle(strval('A').strval($count))
                ->getFont()
                ->setBold(true);
            $spreadsheet->setActiveSheetIndex(0)
                ->getStyle(strval('B').strval($count))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue(strval('B').strval($count), intval(@$choices->firstMatch(['question_id' => $option['question_id'], 'choice_id' => $option['choice_id']])['total']))
                ->getStyle(strval('B').strval($count))
                ->getFont()
                ->setBold(true);
            $spreadsheet->setActiveSheetIndex(0)
                ->getStyle(strval('B').strval($count))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $count++;
        }
    }

}

$count++;
$count++;
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.strval($count), ucwords('Certified true and correct:'))
    ->getStyle('A'.strval($count))
    ->getFont()
    ->setBold(false);

$count++;

$count++;
$count++;
$startHead = 'A';
foreach ($signatories as $signatory){
    $startCount = $count;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue(strval($startHead).strval($startCount), strtoupper($signatory->signatory))
        ->getStyle(strval($startHead).strval($startCount))
        ->getFont()
        ->setBold(true);
    $startCount++;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue(strval($startHead).strval($startCount), ucwords($signatory->position))
        ->getStyle(strval($startHead).strval($startCount))
        ->getFont()
        ->setBold(false);

    $startHead++;
}

// Set custom page size
$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4); // Change to desired paper size
$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE); // Set orientation if needed
$spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1); // Fit to width
$spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0); // Do not fit to height

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(250, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(250, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(250, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(250, 'pt');

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')
    ->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->filename = ucwords($filename);
$export->mime = strtolower('excel');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)) {
// Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
    $writer->save('php://output');
    exit(0);
}