<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Visitor[]|\Cake\Collection\CollectionInterface $visitors
 */
?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'visitor-form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('office_id', ucwords('Office'));?>
                        <?=$this->Form->select('office_id', $offices,[
                            'class' => 'form-control',
                            'id' => 'office-id',
                            'required' => true,
                            'empty' => ucwords('select office'),
                            'title' => ucwords('Please Fill Out This Field')
                        ]);?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('department_id', ucwords('Department'));?>
                        <?=$this->Form->select('department_id', $departments,[
                            'class' => 'form-control',
                            'id' => 'department-id',
                            'required' => true,
                            'empty' => ucwords('select Department'),
                            'title' => ucwords('Please Fill Out This Field')
                        ]);?>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('assist_id',[
                    'id' => 'assist-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ]);?>
                <?=$this->Form->hidden('visitor_id',[
                    'id' => 'visitor-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ]);?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ]);?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Is Visited</th>
                                    <th>Transaction Code</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>Agency/Office</th>
                                    <th>Registered At</th>
                                    <th>Assisted By</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('office/visitors/year');?>


