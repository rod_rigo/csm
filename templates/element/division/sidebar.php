<?php
/**
 * @var \App\View\AppView $this
 */
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="javascript:void(0);" class="brand-link">
        <img src="<?=$this->Url->assetUrl('img/'.(@$logo->logo));?>" alt="SDO Logo" class="brand-image img-thumbnail elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?=ucwords(@$logo->title);?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=$this->Url->assetUrl('/img/user-avatar/'.(strval($auth['id'])).'.png');?>?time=<?=(time());?>" class="img-circle elevation-2" loading="lazy" alt="User Image">
            </div>
            <div class="info">
                <a href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Users', 'action' => 'account', intval(@$auth['id'])]);?>" turbolink class="d-block"><?=@($auth['name']);?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">NAVIGATION</li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('dashboards'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('dashboards'))? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Dashboards', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('dashboards') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Surveys</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Dashboards', 'action' => 'visitors']);?>" class="nav-link <?=(strtolower($controller) == strtolower('dashboards') && strtolower($action) == strtolower('visitors'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Visitors</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header">CONFIGURATION</li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'CustomerTypes', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('customertypes') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Customer Types
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Genders', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('genders') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-genderless"></i>
                        <p>
                            Genders
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Spans', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('spans') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-child"></i>
                        <p>
                            Spans
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Offices', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('offices') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Offices
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Departments', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('departments') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Departments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Services', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('services') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Services
                        </p>
                    </a>
                </li>
                <li class="nav-header">CSM</li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Subjects', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('subjects') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Subjects
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Questions', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('questions') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-question-circle"></i>
                        <p>
                            Questions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Choices', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('Choices') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            Choices
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('visitors'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Visitors
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Visitors', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Visitors', 'action' => 'today']);?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Visitors', 'action' => 'week']);?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Visitors', 'action' => 'month']);?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Visitors', 'action' => 'year']);?>" class="nav-link <?=(strtolower($controller) == strtolower('visitors') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('surveys') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && !in_array(strtolower($action),[strtolower('bin')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-pencil-alt"></i>
                        <p>
                            Surveys
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Surveys', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Surveys', 'action' => 'today']);?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Surveys', 'action' => 'week']);?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Surveys', 'action' => 'month']);?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Surveys', 'action' => 'year']);?>" class="nav-link <?=(strtolower($controller) == strtolower('surveys') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=((in_array(strtolower($controller),[strtolower('others'), strtolower('reports')]) && in_array(strtolower($action),[strtolower('surveys'), strtolower('services')])) || in_array(strtolower($controller),[strtolower('others')]) && in_array(strtolower($action),[strtolower('index')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=((in_array(strtolower($controller),[strtolower('others'), strtolower('reports')]) && in_array(strtolower($action),[strtolower('surveys'), strtolower('services')])) || in_array(strtolower($controller),[strtolower('others')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fa fa-file-excel"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Reports', 'action' => 'surveys']);?>" class="nav-link <?=(strtolower($controller) == strtolower('reports') && strtolower('surveys') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Surveys</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Reports', 'action' => 'services']);?>" class="nav-link <?=(strtolower($controller) == strtolower('reports') && strtolower('services') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Services</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Division', 'controller' => 'Others', 'action' => 'index']);?>" class="nav-link <?=(strtolower($controller) == strtolower('others') && strtolower('index') == strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Others</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header"></li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout']);?>" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
