<?php
/**
 * @var \App\View\AppView $this
 */

$background = \Cake\ORM\TableRegistry::getTableLocator()->get('Backgrounds')
    ->find()
    ->where([
        'is_active =' => intval(1)
    ])
    ->first();

?>

<footer>
<!--    <div class="footer bg-footer section border-bottom">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-4 col-md-4 col-sm-12 col-6 mb-5 mb-md-0">-->
<!--                    <h4 class="text-white mb-5">ABOUT THE DEVELOPERS</h4>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-md-4 col-sm-12 col-6 mb-5 mb-md-0">-->
<!--                    <h4 class="text-white mb-1">Rodrigo Cabotaje Jr</h4>-->
<!--                    <ul class="list-unstyled">-->
<!--                        <li class="mb-3">-->
<!--                            <a class="text-color text-white" href="javascript:void(0);">09100575676</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-md-4 col-sm-12 col-6 mb-5 mb-md-0">-->
<!--                    <h4 class="text-white mb-1">Jeff Calaunan</h4>-->
<!--                    <ul class="list-unstyled">-->
<!--                        <li class="mb-3">-->
<!--                            <a class="text-color text-white" href="javascript:void(0);">09173191479</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="copyright py-4 bg-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 text-sm-left text-center">
                    <p class="mb-0"> All Rights Reserved <?=date('Y');?>.
                        </p>
                </div>
            </div>
        </div>
    </div>
</footer>
