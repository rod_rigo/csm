<?php
/**
 * @var \App\View\AppView $this
 */

$background = \Cake\ORM\TableRegistry::getTableLocator()->get('Backgrounds')
    ->find()
    ->where([
        'is_active =' => intval(1)
    ])
    ->first();

?>

<style>
    #remarks {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
    select.form-control{
        height: 60px !important;
    }
</style>
<script>
    var subjectId = parseInt(<?=intval($subject->id);?>);
</script>

<!-- page title -->
<section class="page-title-section overlay" style="object-fit: contain; background-repeat: no-repeat; background-size: 100% 100%;" data-background="<?=$this->Url->assetUrl('/img/'.(@$background->background));?>">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-inline custom-breadcrumb">
                    <li class="list-inline-item">
                        <a class="h2 text-primary font-secondary" href="javascript:void(0);">
                            <?=ucwords($subject->subject);?>
                        </a>
                    </li>
                    <li class="list-inline-item text-white h3 font-secondary @@nasted"></li>
                </ul>
                <p class="text-lighten">
                    <?=$subject->description?>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- /page title -->

<!-- contact -->
<section class="section bg-gray">
    <div class="container-fluid p-5">

        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title"><?=ucwords($subject->title);?></h2>
            </div>
        </div>

        <?=$this->Form->create($survey,['id' => 'form', 'class' => 'row d-flex justify-content-center align-items-start', 'type' => 'file']);?>
            <div class="col-md-12 col-md-9 col-lg-10">

                <div class="row mt-2 pb-1">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                        <strong>
                            <?=$this->Form->label('customer_types', ucwords('customer type'));?>
                        </strong>
                        <?=$this->Form->select('customer_types', $customerTypes,[
                            'class' => 'form-control form-control-lg',
                            'required' => true,
                            'id' => 'customer-types',
                            'empty' => ucwords('select customer type'),
                            'title' => ucwords('please fill out this field'),
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <?php foreach ($visitors as $index => $visitor):?>
                    <div class="row border-bottom mt-4 pb-2">
                        <h6>
                            <?=strval($index+1);?>)<?=strtoupper($visitor->office->office);?> - <?=strtoupper($visitor->department->department);?>
                        </h6>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label(''.(strval($index)).'.purpose', ucwords('purpose'));?>
                            </strong>
                            <?=$this->Form->textarea(''.(strval($index)).'.purpose',[
                                'class' => 'form-control',
                                'placeholder' => ucwords('enter Your purpose of visit (specify)'),
                                'required' => true,
                                'id' => (strval($index)).'-purpose',
                                'autocomplete' => 'off',
                                'title' => ucwords('please fill out this field'),
                            ]);?>
                            <small></small>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <?php
                            $collection = (new \Cake\Collection\Collection($services))
                                ->match([
                                    'office_id' => intval($visitor->office_id),
                                    'department_id' => intval($visitor->department_id)
                                ])
                                ->map(function($value, $key){
                                    return [
                                        'text' => ucwords($value['service']),
                                        'value' => intval($value['id']),
                                    ];
                                })
                                ->toList();
                            ?>
                            <strong>
                                <?=$this->Form->label(''.(strval($index)).'.service_id', ucwords('service'));?>
                            </strong>
                            <?=$this->Form->select(''.(strval($index)).'.service_id', $collection,[
                                'class' => 'form-control form-control-lg',
                                'required' => true,
                                'id' => ''.(strval($index)).'-service-id',
                                'empty' => ucwords('Select Service'),
                                'title' => ucwords('please fill out this field'),
                            ]);?>
                            <small></small>
                        </div>
                        <?php foreach ($questions as $key => $question):?>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-4 border-bottom my-2 pb-3">
                                <p class="mb-2">
                                    <?=$question->question?>
                                </p>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 d-sm-flex flex-sm-column align-items-sm-start d-md-flex flex-md-row justify-content-md-start align-items-md-center d-lg-flex flex-lg-row justify-content-lg-start align-items-lg-center">
                                        <?php foreach ($question->options as $option):?>
                                            <div class="icheck-primary mx-2">
                                                <div class="icheck-primary d-inline">
                                                    <input type="radio" id="choice-<?=$option->id?>-<?=strval($index).'-'.strval($key);?>" class="choices <?=boolval($question->is_charter)? 'charters-'.(strval($index)): '';?>" points="<?=$option->choice->points?>" value="<?=$option->choice_id?>" data-id="<?=strval($key);?>" data-index="<?=strval($index);?>" data-index-id="<?=strval($index);?>-<?=strval($key);?>" trigger-charter="<?=$option->trigger_charter?>" required>
                                                    <label class="text-dark" for="choice-<?=$option->id?>-<?=strval($index).'-'.strval($key);?>">
                                                        <strong> <?=ucwords($option->choice->choice);?></strong>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                    <?=$this->Form->hidden(''.(strval($index)).'.answers.'.($key).'.question_id',[
                                        'id' => ''.(strval($index)).'-answers-'.($key).'-question-id',
                                        'value' => $question->id,
                                        'required' => true,
                                        'class' => 'answers-question-id',
                                    ]);?>
                                    <small></small>
                                    <?=$this->Form->hidden(''.(strval($index)).'.answers.'.($key).'.choice_id',[
                                        'id' => ''.(strval($index)).'-answers-'.($key).'-choice-id',
                                        'required' => true,
                                        'class' => 'answers-choice-id',
                                        'charter' => $question->is_charter
                                    ]);?>
                                    <small></small>
                                    <?=$this->Form->hidden(''.(strval($index)).'.answers.'.($key).'.points',[
                                        'id' => ''.(strval($index)).'-answers-'.($key).'-points',
                                        'required' => true,
                                        'value' => 0,
                                        'class' => 'answers-points',
                                    ]);?>
                                    <small></small>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <strong>
                                <?=$this->Form->label(''.(strval($index)).'.remarks', ucwords('Suggestions on how we can further improve our services'));?>
                            </strong>
                            <?= $this->Form->textarea(''.(strval($index)).'.remarks',[
                                'class' => 'form-control ck-editor',
                                'placeholder' => ucwords('remarks'),
                                'id' => ''.(strval($index)).'-remarks',
                                'pattern' => '(.){1,}',
                                'title' => ucwords('please fill out this field'),
                                'required' => false,
                                'value' => '-'
                            ]);?>
                            <small></small>
                        </div>
                        <?=$this->Form->hidden(''.(strval($index)).'.score',[
                            'id' => ''.(strval($index)).'-score',
                            'required' => true,
                            'value' => intval(0)
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.office_id',[
                            'id' => ''.(strval($index)).'-office-id',
                            'required' => true,
                            'value' => intval($visitor->office_id)
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.department_id',[
                            'id' => ''.(strval($index)).'-department-id',
                            'required' => true,
                            'value' => intval($visitor->department_id)
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.ca_no',[
                            'id' => ''.(strval($index)).'-ca-no',
                            'required' => true,
                            'value' => $ca_no
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.subject_id',[
                            'id' => ''.(strval($index)).'-subject-id',
                            'required' => true,
                            'value' => $subject->id
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.visitor_id',[
                            'id' => ''.(strval($index)).'-visitor-id',
                            'required' => true,
                            'value' => intval($visitor->id)
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.transaction_code',[
                            'id' => ''.(strval($index)).'-transaction-code',
                            'required' => true,
                            'value' => strval($visitor->transaction_code)
                        ]);?>
                        <?=$this->Form->hidden(''.(strval($index)).'.customer_type_id',[
                            'class' => 'form-control form-control-lg',
                            'placeholder' => ucwords('Customer Type'),
                            'required' => true,
                            'id' => ''.(strval($index)).'-customer-type-id',
                            'title' => ucwords('please fill out this field'),
                            'value' => intval(0)
                        ]);?>
                        <small></small>
                    </div>
                <?php endforeach;?>
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                        <?=$this->Form->button('Submit',[
                            'type' => 'submit',
                            'class' => 'btn btn-success'
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-2" id="pending-transactions">

            </div>
        <?=$this->Form->end();?>
    </div>
</section>
<!-- /contact -->
<?=$this->Html->script('surveys/multiple');?>
