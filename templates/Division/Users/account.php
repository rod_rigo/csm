<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<script>
    var id = parseInt(<?=intval($user->id);?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">User Form</h3>
            </div>
            <?= $this->Form->create($user,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="form-group row">
                    <?=$this->Form->label('name', ucwords('Name'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('name',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Name'),
                            'required' => true,
                            'id' => 'name',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('username', ucwords('username'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('username',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('username'),
                            'required' => true,
                            'id' => 'username',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('email', ucwords('Email'),[
                        'class' => 'col-sm-2 col-form-label'
                    ]);?>
                    <div class="col-sm-10">
                        <?= $this->Form->email('email',[
                            'class' => 'form-control',
                            'placeholder' => ucwords('Email'),
                            'required' => true,
                            'id' => 'email',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => $user->is_active
                ]);?>
                <?= $this->Form->hidden('is_admin',[
                    'id' => 'is-admin',
                    'value' => $user->is_admin
                ]);?>
                <?= $this->Form->hidden('is_division',[
                    'id' => 'is-division',
                    'value' => $user->is_division
                ]);?>
                <?= $this->Form->hidden('is_office',[
                    'id' => 'is-office',
                    'value' => $user->is_office
                ]);?>
                <?= $this->Form->hidden('token',[
                    'id' => 'token',
                    'value' => uniqid()
                ]);?>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ]);?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>


<?=$this->Html->script('division/users/account');?>
