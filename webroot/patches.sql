-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 20, 2024 at 01:35 AM
-- Server version: 8.0.31
-- PHP Version: 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `patches`
--

DROP TABLE IF EXISTS `patches`;
CREATE TABLE IF NOT EXISTS `patches` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `started_at` date NOT NULL,
  `patch` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `patches to users` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `patches`
--

INSERT INTO `patches` (`id`, `user_id`, `started_at`, `patch`, `created`, `modified`, `deleted`) VALUES
(1, 1, '2024-02-08', '<p>Developed CSM</p><p>Features</p><ul><li>Visitor Registration</li><li>Survey</li><li>Users Authentication</li><li>Admin Dashboard</li><li>Survey Configuration</li></ul>', '2024-05-20 09:23:31', '2024-05-20 09:23:31', NULL),
(2, 1, '2024-02-17', '<p>Update ECSM</p><ul><li>Generate Reports (Excel/PDF)</li><li>Adjust Visitor Privacy</li><li>Website Management</li><li>Layout Management</li></ul>', '2024-05-20 09:27:13', '2024-05-20 09:29:10', NULL),
(3, 1, '2024-02-24', '<p>Update CSM Reports</p><ul><li>Question Reports</li><li>Survey Reports</li><li>Services Reports</li></ul>', '2024-05-20 09:27:59', '2024-05-20 09:29:01', NULL),
(4, 1, '2024-04-14', '<p>CSM Update</p><ul><li>New Module For Offices</li><li>Notification Messages</li><li>Visit Control</li><li>Assist Visitors</li></ul>', '2024-05-20 09:31:06', '2024-05-20 09:31:06', NULL),
(5, 1, '2024-04-27', '<p>Update CSM</p><ul><li>Survey Reports For Offices</li><li>Service Reports For Offices</li></ul>', '2024-05-20 09:32:00', '2024-05-20 09:32:00', NULL),
(6, 1, '2024-05-11', '<p>Update CSM&nbsp;</p><ul><li>Activity Logs</li><li>Database Backup</li><li>Export Logs</li></ul>', '2024-05-20 09:32:52', '2024-05-20 09:33:15', NULL),
(7, 1, '2024-05-19', '<p>Update CSM</p><ul><li>Adjust UI</li><li>Manage Navigation Headers</li><li>Make Multiple Surveys</li><li>Office And Department Legend</li></ul>', '2024-05-20 09:35:04', '2024-05-20 09:35:04', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `patches`
--
ALTER TABLE `patches`
  ADD CONSTRAINT `patches to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
