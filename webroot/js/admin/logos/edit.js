'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;

    var cropper;
    var zoom = 0;
    var scaleY = false;
    var scaleX = false;

    cropper = new Cropper( document.querySelector('#cropper'), {
        dragMode: 'move',
        aspectRatio: 'free',
        initialAspectRatio: 'free',
        autoCrop:true,
        responsive:true,
        restore:true,
        checkOrientation:true,
        modal:true,
        guides:true,
        center:true,
        highlight:true,
        background:false,
        autoCropArea: 0.8,
        movable:true,
        rotatable:true,
        scalable:true,
        zoomable:true,
        zoomOnTouch:true,
        zoomOnWheel:true,
        wheelZoomRatio:true,
        cropBoxMovable:true,
        cropBoxResizable:true,
        toggleDragModeOnDblclick:true,
        minContainerWidth:200,
        minContainerHeight:200,
        minCanvasWidth:200,
        minCanvasHeight:200,
        minCropBoxWidth:50,
        minCropBoxHeight:50,
        ready:function (e) {

        },
        cropstart:function (e) {

        },
        cropmove:function (e) {

        },
        cropend:function (e) {

        },
        crop:function (e) {

        },
        zoom:function (e) {

        }
    });

    $('#crop').click(function () {
        cropper.setDragMode('crop');
    });

    $('#move').click(function () {
        cropper.setDragMode('move');
    });

    $('#zoom-in').click(function (e) {
        cropper.zoom(parseFloat(0.5));
    });

    $('#zoom-out').click(function (e) {
        cropper.zoom(-parseFloat(0.5));
    });

    $('#rotate-left').click(function (e) {
        cropper.rotate(-parseFloat(90));
    });

    $('#rotate-right').click(function (e) {
        cropper.rotate(parseFloat(90));
    });

    $('#flip-vertical').click(function (e) {
        if(scaleY){
            cropper.scaleY(1);
            scaleY = false;
        }else{
            cropper.scaleY(-1);
            scaleY = true;
        }
    });

    $('#flip-horizontal').click(function (e) {
        if(scaleX){
            cropper.scaleX(1);
            scaleX = false;
        }else{
            cropper.scaleX(-1);
            scaleX = true;
        }
    });

    $('#move-up').click(function (e) {
        cropper.move(parseInt(0), -parseInt(10));
    });

    $('#move-down').click(function (e) {
        cropper.move(parseInt(0), parseInt(10));
    });

    $('#move-left').click(function (e) {
        cropper.move(-parseInt(10), parseInt(0));
    });

    $('#move-right').click(function (e) {
        cropper.move(parseInt(10), parseInt(0));
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#image-preview').attr('src', '#');
    });

    $('#complete').click(function (e) {
        var src = cropper.getCroppedCanvas({
            maxWidth: parseInt($('#width').val()),
            maxHeight: parseInt($('#height').val()),
            fillColor:'#ffffff',
        }).toDataURL('image/jpeg');
        $('#image-preview').attr('src',src);
        $('#logo').val(src);
        $('#modal').modal('toggle');
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('input[type="file"]').change(function (e) {
        var file = e.target.files[0];
        var mimes = ['image/png', 'image/jpeg', 'image/jpg', 'image/jfif', 'image/webp'];
        var reader = new FileReader();

        if(!mimes.includes(file.type)){
            $(this).addClass('is-invalid').parent().parent().next('small').text('Invalid Image Format');
            return true;
        }

        reader.addEventListener('load', function (e) {
            if(cropper){
                cropper.destroy();
            }
            $('#cropper').attr('src', e.target.result);
            cropper = new Cropper( document.querySelector('#cropper'), {
                dragMode: 'move',
                aspectRatio: 'free',
                initialAspectRatio: 'free',
                autoCrop:true,
                responsive:true,
                restore:true,
                checkOrientation:true,
                modal:true,
                guides:true,
                center:true,
                highlight:true,
                background:false,
                autoCropArea: 0.8,
                movable:true,
                rotatable:true,
                scalable:true,
                zoomable:true,
                zoomOnTouch:true,
                zoomOnWheel:true,
                wheelZoomRatio:true,
                cropBoxMovable:true,
                cropBoxResizable:true,
                toggleDragModeOnDblclick:true,
                minContainerWidth:200,
                minContainerHeight:200,
                minCanvasWidth:200,
                minCanvasHeight:200,
                minCropBoxWidth:50,
                minCropBoxHeight:50,
                ready:function (e) {

                },
                cropstart:function (e) {

                },
                cropmove:function (e) {

                },
                cropend:function (e) {

                },
                crop:function (e) {

                },
                zoom:function (e) {

                }
            });
        });

        $(this).removeClass('is-invalid').parent().parent().next('small').empty();
        $(this).next('label.custom-file-label').text(file.name);
        reader.readAsDataURL(file);
    });

    $('#title').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#height').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#width').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

});