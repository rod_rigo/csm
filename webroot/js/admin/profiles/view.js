'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'profiles/edit/'+(userId)+'/'+(profileId);

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
                $.map(value.profile, function (value, key) {
                    $('[name="profile['+(key)+']"]').addClass('is-invalid');
                    $('[name="profile['+(key)+']"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#name').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Enter A Name');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#username').on('input', function (e) {
        var regex = null;
        var value = null;

        regex = /^(.){1,}$/;
        value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Enter A Name');
            return true;
        }

        regex = /^(.[^\s])?[\w]{1,}$/;
        value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Use Underscore (_) For Space');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#email').on('input', function (e) {
        var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please @ In Email');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

    $('#admin').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-admin').val(Number(checked));
    });

    $('#division').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-division').val(Number(checked));
    });

    $('#office').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-office').val(Number(checked));
    });

    $('#profile-office-id').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Select A Office');
            return true;
        }

        getDepartmentsList(value);

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#profile-department-id').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Select A Department');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    function getDepartmentsList(officeId) {
        $.ajax({
            url: mainurl+'departments/getDepartmentsList/'+(officeId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#profile-department-id').empty().prop('disabled', true).append('<option value="">Please Wait</option>');
            },
        }).done(function (data, status, xhr) {
            var department = $('#profile-department-id');
            department.empty();
            department.append('<option value="">Select Department</option>');
            $.map(data, function (data, key) {
                department.append('<option value="'+(key)+'">'+(data)+'</option>');
            });
            department.prop('disabled', false);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
        });
    }

});