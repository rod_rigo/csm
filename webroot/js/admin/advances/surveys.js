'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'advances/';
    var url = '';
    var startDate = moment($('#start-date').val()).format('Y-MM-DD');
    var endDate = moment($('#end-date').val()).format('Y-MM-DD');
    var records = $('#records').val();

    google.load('visualization', '1', {
        packages:['corechart', 'charteditor'],
    });

    var derivers = $.pivotUtilities.derivers;
    var renderers = $.extend(
        $.pivotUtilities.renderers,
        $.pivotUtilities.c3_renderers,
        $.pivotUtilities.d3_renderers,
        $.pivotUtilities.plotly_renderers,
        $.pivotUtilities.gchart_renderers,
        $.pivotUtilities.export_renderers
    );


    $('#form').submit(function (e) {
        e.preventDefault();
        $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', true);
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = $('#records').val();
        pivot(baseurl+'getSurveys?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records));
    });

    function pivot(url) {
        $.ajax({
            url:url,
            type:'GET',
            method:'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Loading Pivot Table, Please Wait!',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
            }
        }).done(function (data, status, xhr) {
            $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', false);
            $('#pivot').pivotUI(data, {
                derivedAttributes: {
                    'gender': function(data) {
                        return (data.gender).toUpperCase();
                    },
                    'span': function(data) {
                        return (data.span).toUpperCase();
                    },
                    'office': function(data) {
                        return (data.office).toUpperCase();
                    },
                    'department': function(data) {
                        return (data.department).toUpperCase();
                    },
                    'service': function(data) {
                        return (data.service).toUpperCase();
                    },
                    'customer_type': function(data) {
                        return (data.customer_type).toUpperCase();
                    },
                },
                onRefresh: function(e) {

                },
                rendererOptions: {
                    table: {
                        clickCallback: function(e, value, filters, pivotData){

                        },
                    },
                },
                renderers: renderers,
                cols: ['department'],
                rows: ['office'],
                vals: ['gender'],
                rendererName: 'Table',
                aggregatorName: 'Count',
                rowOrder: 'value_z_to_a',
                colOrder: 'value_z_to_a',
            });
            Swal.close();
        }).fail(function (data, status, xhr) {

        });
    }

    pivot(baseurl+'getSurveys');

});
