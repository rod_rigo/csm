$(document).ready(function (e) {
    const baseurl = mainurl+'advances/';
    var url = '';
    var colspan = 8;
    var headers = [
        ('NO').toUpperCase(),
        ('REFERENCE CODE').toUpperCase(),
        ('DATE').toUpperCase(),
        ('NAME').toUpperCase(),
        ('CLIENT TYPE').toUpperCase(),
        ('SEX').toUpperCase(),
        ('AGE').toUpperCase(),
        ('REGION OF RESIDENCE').toUpperCase(),
    ];
    var columns = [
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
        {
            type: 'text',
            wordWrap:true,
            width:'400',
        },
    ];
    var startDate = moment($('#start-date').val()).format('Y-MM-DD');
    var endDate = moment($('#end-date').val()).format('Y-MM-DD');
    var records = $('#records').val();
    var title = function () {
        return 'Questions Of '+(moment($('#start-date').val()).format('Y-MM-DD'))+' - '+(moment($('#end-date').val()).format('Y-MM-DD'));
    };

    $('#form').submit(function (e) {
        e.preventDefault();
        $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', true);
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = $('#records').val();
        getQuestions(baseurl+'getQuestions?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records));
    });

    $.map(questions, function (data, key) {
        colspan++;
        headers.push((data.title).toUpperCase());
        columns.push({
            type: 'text',
            wordWrap:true,
            width:'400',
        });
        $.map(data.options, function (data, key) {
            colspan++;
            headers.push((data.position).toString().toUpperCase());
            columns.push({
                type: 'text',
                wordWrap:true,
                width:'400',
            });
        });
    });

    var sheet = [
        headers
    ];

    var table = jexcel(document.getElementById('spreadsheet'),{
        data: sheet,
        minDimensions:[26, 50],
        filters: true,
        allowComments:true,
        tableOverflow: true,
        lazyLoading: true,
        tableWidth: '100%',
        tableHeight: '100em',
        defaultColWidth: 100,
        freezeColumns: 0,
        columnDrag:true,
        columnSorting:true,
        rowResize: true,
        loadingSpin:true,
        csvFileName: title(),
        includeHeadersOnDownload: false,
        search: true,
        columns: columns,
        nestedHeaders:[
            [
                {
                    title: 'Questions Report',
                    colspan: parseInt(colspan),
                },
            ],
        ],
        mergeCells:{
//                A1:[3,4],
        },
        footers: [
//                ['Total','=SUM(I1:I100)']
        ],
        toolbar:[
            {
                type: 'i',
                content: 'undo',
                onclick: function() {
                    table.undo();
                }
            },
            {
                type: 'i',
                content: 'redo',
                onclick: function() {
                    table.redo();
                }
            },
            {
                type: 'i',
                content: 'save',
                onclick: function () {
                    Swal.fire({
                        title: 'Save As CSV',
                        text: 'Are You Sure?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            table.download(false);
                        }
                    });
                }
            },
            {
                type: 'i',
                content: 'data_object',
                onclick: function () {
                    var blob = new Blob([JSON.stringify(table.getData())], {
                        type: 'application/json'
                    });
                    Swal.fire({
                        title: 'Download As JSON',
                        text: 'Are You Sure?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            saveAs(blob, (title())+'.json');
                        }
                    });
                }
            },
            {
                type: 'select',
                k: 'font-family',
                v: ['Arial','Verdana']
            },
            {
                type: 'select',
                k: 'font-size',
                v: ['9px','10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px']
            },
            {
                type: 'i',
                content: 'format_align_left',
                k: 'text-align',
                v: 'left'
            },
            {
                type:'i',
                content:'format_align_center',
                k:'text-align',
                v:'center'
            },
            {
                type: 'i',
                content: 'format_align_right',
                k: 'text-align',
                v: 'right'
            },
            {
                type: 'i',
                content: 'format_bold',
                k: 'font-weight',
                v: 'bold'
            },
            {
                type: 'color',
                content: 'format_color_text',
                k: 'color'
            },
            {
                type: 'color',
                content: 'format_color_fill',
                k: 'background-color'
            },
            {
                type: 'i',
                content: 'open_in_full',
                k: 'toggle-screen',
                id: 'toggle-screen',
                v: 'center',
                onclick: function () {
                    if($('#toggle-screen').hasClass('is-full-screen')){
                        table.fullscreen(false);
                        $('#toggle-screen').text('open_in_full').removeClass('is-full-screen');
                    }else{
                        table.fullscreen(true);
                        $('#toggle-screen').text('close_fullscreen').addClass('is-full-screen');
                    }
                }
            },
        ],
        contextMenu: function(obj, x, y, e) {
            var items = [];

            if (y == null) {
                // Insert a new column
                if (obj.options.allowInsertColumn == true) {
                    items.push({
                        title:obj.options.text.insertANewColumnBefore,
                        onclick:function() {
                            obj.insertColumn(1, parseInt(x), 1);
                        }
                    });
                }

                if (obj.options.allowInsertColumn == true) {
                    items.push({
                        title:obj.options.text.insertANewColumnAfter,
                        onclick:function() {
                            obj.insertColumn(1, parseInt(x), 0);
                        }
                    });
                }

                // Delete a column
                if (obj.options.allowDeleteColumn == true) {
                    items.push({
                        title:obj.options.text.deleteSelectedColumns,
                        onclick:function() {
                            obj.deleteColumn(obj.getSelectedColumns().length ? undefined : parseInt(x));
                        }
                    });
                }

                // Rename column
                if (obj.options.allowRenameColumn == true) {
                    items.push({
                        title:obj.options.text.renameThisColumn,
                        onclick:function() {
                            obj.setHeader(x);
                        }
                    });
                }

                // Sorting
                if (obj.options.columnSorting == true) {
                    // Line
                    items.push({ type:'line' });

                    items.push({
                        title:obj.options.text.orderAscending,
                        onclick:function() {
                            obj.orderBy(x, 0);
                        }
                    });
                    items.push({
                        title:obj.options.text.orderDescending,
                        onclick:function() {
                            obj.orderBy(x, 1);
                        }
                    });
                }
            } else {
                // Insert new row
                if (obj.options.allowInsertRow == true) {
                    items.push({
                        title:obj.options.text.insertANewRowBefore,
                        onclick:function() {
                            obj.insertRow(1, parseInt(y), 1);
                        }
                    });

                    items.push({
                        title:obj.options.text.insertANewRowAfter,
                        onclick:function() {
                            obj.insertRow(1, parseInt(y));
                        }
                    });
                }

                if (obj.options.allowDeleteRow == true) {
                    items.push({
                        title:obj.options.text.deleteSelectedRows,
                        onclick:function() {
                            obj.deleteRow(obj.getSelectedRows().length ? undefined : parseInt(y));
                        }
                    });
                }

                if (x) {
                    if (obj.options.allowComments == true) {
                        items.push({ type:'line' });

                        var title = obj.records[y][x].getAttribute('title') || '';

                        items.push({
                            title: title ? obj.options.text.editComments : obj.options.text.addComments,
                            onclick:function() {
                                obj.setComments([ x, y ], prompt(obj.options.text.comments, title));
                            }
                        });

                        if (title) {
                            items.push({
                                title:obj.options.text.clearComments,
                                onclick:function() {
                                    obj.setComments([ x, y ], '');
                                }
                            });
                        }
                    }
                }
            }

            // Line
            items.push({ type:'line' });

            // Save
            if (obj.options.allowExport) {
                items.push({
                    title: obj.options.text.saveAs,
                    shortcut: 'Ctrl + S',
                    onclick: function () {
                        obj.download();
                    }
                });
            }

            // About
            if (obj.options.about) {
                items.push({
                    title:obj.options.text.about,
                    onclick:function() {
                        alert(obj.options.about);
                    }
                });
            }

            return items;
        },
        updateTable:function(instance, cell, col, row, val, label, cellName) {
            if(parseInt(row) == parseInt(0)){
                $(cell).css('background', '#98FF98');
            }
        },
    });

    function getQuestions(url) {
        var array = [
            headers
        ];
        $.ajax({
            url:url,
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend:function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Preparing Rows, Please Wait!',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                var answers = data.answers;
                var cell = [
                    data.transaction_code,
                    data.ca_no,
                    moment(data.created).format('Y-MM-DD'),
                    data.visitor.name,
                    data.customer_type.customer_type,
                    data.visitor.gender.gender,
                    data.visitor.age,
                    data.visitor.agency
                ];
                $.map(questions, function (data, key) {
                    var questionId = parseInt(data.id);
                    cell.push($(data.question).text());
                    $.map(data.options, function (data, key) {
                        var choiceId = parseInt(data.choice_id);
                        var points = '';
                        $.map(answers, function (data, key) {
                            if(parseInt(questionId) == data.question_id && parseInt(choiceId) == data.choice_id){
                                points = '1';
                            }
                        });
                        cell.push(points);
                    });
                });

                array.push(cell);
            });
            table.setData(array);
            $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', false);
            Swal.close();
        }).fail(function (data, status, xhr) {

        });
    }

    getQuestions(baseurl+'getQuestions?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records));

});