'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'others/';
    var url = '';

    var questionIds = [];

    var select = $('#question').select2({
        placeholder: 'Question',
        allowClear: true,
        multiple: true,
        dropdownAutoWidth:true,
        width: '100%'
    });

    select.on('select2:select select2:unselect', function (e) {
        questionIds = $('#question').val();
        questionIds = questionIds.join(',');
    });

    $('.export').click(function (e) {
        var dataId = $(this).attr('data-id');
        var dataUrl = $(this).attr('data-url');
        var filename = $('#filename-'+(parseInt(dataId))+'').val();
        var year = $('#year').val();
        var month = $('#month').val();
        url = (dataUrl)+'?year='+(parseInt(year))+'&month='+(month)+'&question_ids='+(questionIds)+'&filename='+(filename);
        Swal.fire({
            title: 'Export Data',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(baseurl+url);
            }
        });
    });

});