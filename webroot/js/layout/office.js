'use strict';

var sound = new Howl({
    src: ['/csm/audio/audio.mp3'],
    autoplay: false,
    loop: false,
    volume: 1.0,
    onend: function() {
        console.log('Finished!');
    }
});

function notification() {
    $.ajax({
        url: mainurl+'visitors/getNotifications',
        type: 'GET',
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function (e) {
            $('#notification-list-count').text(0);
        },
    }).done(function (data, status, xhr) {
        $('#notification-list-count').text(parseInt(data.length))
            .removeClass('opacity-full')
            .removeClass('opacity-0')
            .addClass(((data.length)? 'opacity-full': 'opacity-0'));
        $.map(data, function (data, key) {
            $('#notification-list').append('<a href="javascript:void(0);" class="dropdown-item"> ' +
                '<i class="fas fa-user mr-2"></i> '+(data.transaction_code)+' ' +
                '<span class="float-right text-muted text-sm">'+(moment(data.created).fromNow(true))+'</span>' +
                ' </a> <div class="dropdown-divider"></div>');
        });
    }).fail(function (data, status, xhr) {

    });
}

websocket.onmessage = function(e) {
    var visitor = JSON.parse(e.data);
    if(parseInt(officeId) == parseInt(visitor.office_id) && parseInt(departmentId) == parseInt(visitor.department_id)){
        notification();
        sound.play();
    }
};

notification();
