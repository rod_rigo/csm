<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * Others Controller
 *
 * @method \App\Model\Entity\Other[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OthersController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $questions = TableRegistry::getTableLocator()->get('Questions')
            ->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->title);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->order(['position' => 'ASC'], true);
        $this->set(compact('questions'));
    }

    public function questions(){
        set_time_limit(500);

        $year = $this->request->getQuery('year')? [
            'year =' => intval($this->request->getQuery('year'))
        ]: null;
      
        $month = ($this->request->getQuery('month'))? [
            'month like' => ucwords($this->request->getQuery('month'))
        ]: null;
        $questionIds = ($this->request->getQuery('question_ids'))? explode(',', $this->request->getQuery('question_ids'))
            : [0];
        $filename = ($this->request->getQuery('filename'))? $this->request->getQuery('filename'): date('Y_F_D');

        $answers = TableRegistry::getTableLocator()->get('Answers')
            ->find()
            ->join([
                'surveys' => [
                    'table' => 'surveys',
                    'type' => 'LEFT',
                    'conditions' => [
                        'surveys.id = Answers.survey_id'
                    ]
                ],
                'offices' => [
                    'table' => 'offices',
                    'type' => 'LEFT',
                    'conditions' => [
                        'offices.id = surveys.office_id'
                    ]
                ],
            ])
            ->select([
                'month' => 'MONTHNAME(Answers.created)',
                'year' => 'YEAR(Answers.created)',
                'office_id' => 'surveys.office_id',
                'choice_id' => 'Answers.choice_id',
                'question_id' => 'Answers.question_id',
                'total' => 'COUNT(Answers.id)',
            ])
            ->group([
                'office_id',
                'choice_id',
                'question_id',
                'month',
                'survey_id',
            ])
            ->having($year)
            ->having($month)
            ->disableHydration()
            ->toArray();

        $questions = TableRegistry::getTableLocator()->get('Questions')
            ->find()
            ->where([
                'id NOT IN' => $questionIds
            ])
            ->contain([
                'Options' => [
                    'queryBuilder' => function($query){
                        return $query->find('all')
                            ->order(['Options.position' => 'ASC'], true);
                    }
                ],
                'Options.Choices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->order(['Questions.position' => 'ASC'],true);

        $options = TableRegistry::getTableLocator()->get('Options')
            ->find()
            ->select([
                'total' => 'COUNT(question_id)',
                'question_id'
            ])
            ->group([
                'question_id',
            ])
            ->enableHydration(false)
            ->toArray();

        $offices = TableRegistry::getTableLocator()->get('Offices')
            ->find()
            ->where([
                'is_active =' => intval(1),
            ]);

        $this->set(compact('answers', 'questions', 'options', 'offices', 'filename'));
    }

    public function csm(){

        $year = $this->request->getQuery('year')? [
            'year =' => intval($this->request->getQuery('year'))
        ]: null;
        $month = ($this->request->getQuery('month'))? [
            'month like' => '%'.ucwords($this->request->getQuery('month')).'%'
        ]: null;
        $questionIds = ($this->request->getQuery('question_ids'))? explode(',', $this->request->getQuery('question_ids'))
            : [0];
        $filename = ($this->request->getQuery('filename'))? $this->request->getQuery('filename'): date('Y_F_D');

        $answers = TableRegistry::getTableLocator()->get('Answers')
            ->find()
            ->join([
                'surveys' => [
                    'table' => 'surveys',
                    'type' => 'LEFT',
                    'conditions' => [
                        'surveys.id = Answers.survey_id'
                    ]
                ],
                'offices' => [
                    'table' => 'offices',
                    'type' => 'LEFT',
                    'conditions' => [
                        'offices.id = surveys.office_id'
                    ]
                ],
                'departments' => [
                    'table' => 'departments',
                    'type' => 'LEFT',
                    'conditions' => [
                        'departments.id = surveys.department_id'
                    ]
                ],
            ])
            ->select([
                'month' => 'MONTHNAME(Answers.created)',
                'year' => 'YEAR(Answers.created)',
                'office_id' => 'surveys.office_id',
                'department_id' => 'surveys.department_id',
                'choice_id' => 'Answers.choice_id',
                'question_id' => 'Answers.question_id',
                'total' => 'COUNT(Answers.id)',
            ])
            ->group([
                'office_id',
                'question_id',
                'department_id',
                'choice_id',
                'month'
            ])
            ->having($year)
            ->having($month)
            ->disableHydration()
            ->toArray();

        $surveys = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->select([
                'office_id',
                'department_id',
                'year' => 'YEAR(Surveys.created)',
                'month' => 'MONTHNAME(Surveys.created)',
                'total' => 'COUNT(Surveys.id)'
            ])
            ->group([
                'office_id',
                'department_id',
                'month'
            ])
            ->having($year)
            ->having($month)
            ->disableHydration()
            ->toArray();

        $offices = TableRegistry::getTableLocator()->get('Offices')
            ->find()
            ->contain([
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->where([
                'is_active =' => intval(1)
            ])
            ->order(['office' => 'ASC'], true);

        $questions = TableRegistry::getTableLocator()->get('Questions')
            ->find()
            ->contain([
                'Options' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->where([
                'is_active =' => intval(1),
                'id NOT IN' => $questionIds
            ])
            ->order(['position' => 'ASC'], true)
            ->disableHydration()
            ->toArray();

        $this->set(compact('filename', 'offices', 'questions', 'answers', 'surveys'));
    }

}
