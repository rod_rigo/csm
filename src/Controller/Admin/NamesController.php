<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * Names Controller
 *
 * @property \App\Model\Table\NamesTable $Names
 * @method \App\Model\Entity\Name[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NamesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    public function recalibrate(){
        $connection = ConnectionManager::get('default');

        $connection->begin();

        $visitors = TableRegistry::getTableLocator()->get('Visitors')->find()
            ->select([
                'name',
                'email',
                'age',
                'gender_id'
            ])
            ->group([
                'email'
            ])
            ->disableHydration()
            ->toArray();

        try{

            $connection->execute('SET FOREIGN_KEY_CHECKS = 0;');
            $connection->execute('TRUNCATE names;');

            $names = $this->Names->newEntities($visitors);
            $names = $this->Names->saveMany($names);

            $connection->commit();
            $this->Flash->success(ucwords('names has been calibrated'));
            return $this->redirect(['prefix' => 'Admin', 'controller' => 'Settings', 'action' => 'index']);

        }catch (\Exception $exception){
            $connection->rollback();
            $this->Flash->error(ucwords('names has not been calibrated'));
            return $this->redirect(['prefix' => 'Admin', 'controller' => 'Settings', 'action' => 'index']);
        }
    }

}
