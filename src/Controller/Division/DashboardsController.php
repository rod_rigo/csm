<?php
declare(strict_types=1);

namespace App\Controller\Division;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Moment\Moment;

/**
 * Dashboards Controller
 *
 * @method \App\Model\Entity\Dashboard[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashboardsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('division');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_division'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    public function index(){

    }

    public function count(){

        $offices = TableRegistry::getTableLocator()->get('Offices')->find()->count();
        $services = TableRegistry::getTableLocator()->get('Services')->find()->count();
        $customerTypes = TableRegistry::getTableLocator()->get('CustomerTypes')->find()->count();

        $collection = (new Collection([
            [
                'offices' => $offices,
                'services' => $services,
                'customer_types' => $customerTypes
            ]
        ]));

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection->first()));
    }

    public function getSurveysCount(){

        $query = TableRegistry::getTableLocator()->get('Surveys');

        $today = $query->find()
            ->where([
                'created >=' => (new Moment(null,'Asia/Manila'))->format('Y-m-d'),
                'created <=' => (new Moment(null,'Asia/Manila'))->addDays(1)->format('Y-m-d')
            ])->count();

        $week = $query->find()
            ->where([
                'created >=' => (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d'),
                'created <=' => (new Moment(null,'Asia/Manila'))->endOf('week')->addDays(1)->format('Y-m-d')
            ])->count();

        $month = $query->find()
            ->where([
                'created >=' => (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d'),
                'created <=' => (new Moment(null,'Asia/Manila'))->endOf('month')->addDays(1)->format('Y-m-d')
            ])->count();

        $year = $query->find()
            ->where([
                'created >=' => (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
                'created <=' => (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d')
            ])->count();


        $collection = (new Collection([
            [
                'today' => $today,
                'week' => $week,
                'month' => $month,
                'year' => $year,
            ]
        ]))->first();

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection));
    }

    public function getSurveys(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->select([
                'total' => 'COUNT(id)',
                'month' => 'MONTHNAME(created)'
            ])
            ->where([
                'created >=' => $startDate,
                'created <=' => $endDate,
            ])
            ->group([
                'month'
            ])
            ->order([
                'created' => 'ASC'
            ], true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getSurveyOffices(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->contain([
                'Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'total' => 'COUNT(Surveys.office_id)',
                'office' => 'UPPER(Offices.legend)'
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->group([
                'office_id'
            ])
            ->order([
                'office' => 'ASC'
            ], true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getSurveyDepartments(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->contain([
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'total' => 'COUNT(Surveys.department_id)',
                'department' => '(CASE WHEN CONCAT(Departments.legend) LIKE "N/A" THEN CONCAT(Offices.legend) ELSE CONCAT(Departments.legend) END)'

            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->group([
                'department_id'
            ])
            ->having([
                'department NOT LIKE' => '%N/A%'
            ])
            ->order([
                'department' => 'ASC'
            ], true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getSurveyCustomerTypes(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->contain([
                'CustomerTypes' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->select([
                'total' => 'COUNT(Surveys.customer_type_id)',
                'customer_type' => 'CustomerTypes.customer_type'
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->group([
                'customer_type_id'
            ])
            ->order([
                'customer_type' => 'ASC'
            ], true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getSurveyGenders(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->join([
                'visitors' => [
                    'table' => 'visitors',
                    'type' => 'LEFT',
                    'conditions' => [
                        'visitors.id = Surveys.visitor_id'
                    ]
                ],
                'genders' => [
                    'table' => 'genders',
                    'type' => 'LEFT',
                    'conditions' => [
                        'genders.id = visitors.gender_id'
                    ]
                ],
            ])
            ->select([
                'total' => 'COUNT(visitors.gender_id)',
                'gender' => 'genders.gender'
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->group([
                'visitors.gender_id'
            ])
            ->order([
                'gender' => 'ASC'
            ],true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getSurveySpans(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Surveys')
            ->find()
            ->join([
                'visitors' => [
                    'table' => 'visitors',
                    'type' => 'LEFT',
                    'conditions' => [
                        'visitors.id = Surveys.visitor_id'
                    ]
                ],
                'spans' => [
                    'table' => 'spans',
                    'type' => 'LEFT',
                    'conditions' => [
                        'spans.id = visitors.span_id'
                    ]
                ],
            ])
            ->select([
                'total' => 'COUNT(visitors.span_id)',
                'span' => 'spans.span'
            ])
            ->where([
                'Surveys.created >=' => $startDate,
                'Surveys.created <=' => $endDate,
            ])
            ->group([
                'visitors.span_id'
            ])
            ->order([
                'span' => 'ASC'
            ],true);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function visitors(){

    }

    public function getVisitorsCount(){

        $query = TableRegistry::getTableLocator()->get('Visitors');

        $today = $query->find()
            ->where([
                'Visitors.created >=' => (new Moment(null,'Asia/Manila'))->format('Y-m-d'),
                'Visitors.created <=' => (new Moment(null,'Asia/Manila'))->addDays(1)->format('Y-m-d'),
                'Visitors.is_cloned =' => intval(0)
            ])->count();

        $week = $query->find()
            ->where([
                'Visitors.created >=' => (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d'),
                'Visitors.created <=' => (new Moment(null,'Asia/Manila'))->endOf('week')->addDays(1)->format('Y-m-d'),
                'Visitors.is_cloned =' => intval(0)
            ])->count();

        $month = $query->find()
            ->where([
                'Visitors.created >=' => (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d'),
                'Visitors.created <=' => (new Moment(null,'Asia/Manila'))->endOf('month')->addDays(1)->format('Y-m-d'),
                'Visitors.is_cloned =' => intval(0)
            ])->count();

        $year = $query->find()
            ->where([
                'Visitors.created >=' => (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
                'Visitors.created <=' => (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d'),
                'Visitors.is_cloned =' => intval(0)
            ])->count();


        $collection = (new Collection([
            [
                'today' => $today,
                'week' => $week,
                'month' => $month,
                'year' => $year,
            ]
        ]))->first();

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($collection));
    }

    public function getVisitors(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Visitors')
            ->find()
            ->select([
                'total' => 'COUNT(id)',
                'month' => 'MONTHNAME(created)',
            ])
            ->where([
                'Visitors.created >=' => $startDate,
                'Visitors.created <=' => $endDate,
                'Visitors.is_cloned =' => intval(0)
            ])
            ->group(['month'])
            ->order(['created' => 'ASC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getVisitorGenders(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Visitors')
            ->find()
            ->contain([
                'Genders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'total' => 'COUNT(gender_id)',
                'gender' => 'Genders.gender'
            ])
            ->where([
                'Visitors.created >=' => $startDate,
                'Visitors.created <=' => $endDate,
                'Visitors.is_cloned =' => intval(0)
            ])
            ->group(['gender_id']);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getVisitorSpans(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Visitors')
            ->find()
            ->contain([
                'Spans' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'total' => 'COUNT(gender_id)',
                'span' => 'Spans.span'
            ])
            ->where([
                'Visitors.created >=' => $startDate,
                'Visitors.created <=' => $endDate,
                'Visitors.is_cloned =' => intval(0)
            ])
            ->group([
                'span_id'
            ])
            ->order([
                'span' => 'ASC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getVisitorCustomerTypes(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Visitors')
            ->find()
            ->join([
                'surveys' => [
                    'table' => 'surveys',
                    'type' => 'INNER',
                    'conditions' => [
                        'surveys.visitor_id = Visitors.id'
                    ],
                ],
                'customer_types' => [
                    'table' => 'customer_types',
                    'type' => 'INNER',
                    'conditions' => [
                        'customer_types.id = surveys.customer_type_id'
                    ],
                ],
            ])
            ->select([
                'customer_type' => 'customer_types.customer_type',
                'total' => 'COUNT(customer_types.id)',
            ])
            ->where([
                'Visitors.created >=' => $startDate,
                'Visitors.created <=' => $endDate,
                'Visitors.is_cloned =' => intval(0)
            ])
            ->group([
                'customer_type'
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getVisitorOffices(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Visitors')
            ->find()
            ->contain([
                'Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'total' => 'COUNT(Visitors.office_id)',
                'office' => 'UPPER(Offices.legend)',
            ])
            ->where([
                'Visitors.created >=' => $startDate,
                'Visitors.created <=' => $endDate,
                'Visitors.is_cloned =' => intval(0)
            ])
            ->group([
                'Visitors.office_id'
            ])
            ->order([
                'office' => 'ASC'
            ], true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function getVisitorDepartments(){
        $startDate = ($this->request->getQuery('start_date'))?
            (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))?
            (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->addDays(1)->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = TableRegistry::getTableLocator()->get('Visitors')
            ->find()
            ->contain([
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->select([
                'total' => 'COUNT(Visitors.department_id)',
                'department' => 'UPPER(Departments.legend)',
            ])
            ->where([
                'Visitors.created >=' => $startDate,
                'Visitors.created <=' => $endDate,
                'Visitors.is_cloned =' => intval(0)
            ])
            ->group([
                'Visitors.department_id'
            ])
            ->order([
                'department' => 'ASC'
            ], true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

}
