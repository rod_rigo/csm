<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Visitors Model
 *
 * @property \App\Model\Table\SurveysTable&\Cake\ORM\Association\HasMany $Surveys
 *
 * @method \App\Model\Entity\Visitor newEmptyEntity()
 * @method \App\Model\Entity\Visitor newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Visitor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Visitor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Visitor findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Visitor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Visitor[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Visitor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Visitor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Visitor[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VisitorsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('visitors');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Spans', [
            'foreignKey' => 'span_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Surveys', [
            'foreignKey' => 'visitor_id',
            'joinType'   => 'INNER'
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
    }


    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        if($entity->isNew() && !boolval($entity->is_cloned)){
            $entity->logical_id = intval($entity->id);
            $this->save($entity,['atomic' => false]);
        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');
        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, true);

        $validator
            ->scalar('logical_id')
            ->numeric('logical_id')
            ->requirePresence('logical_id', true)
            ->notEmptyString('logical_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('assist_id')
            ->numeric('assist_id')
            ->requirePresence('assist_id', true)
            ->notEmptyString('assist_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('office_id')
            ->numeric('office_id')
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('department_id')
            ->numeric('department_id')
            ->requirePresence('department_id', true)
            ->notEmptyString('department_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('gender_id')
            ->numeric('gender_id')
            ->requirePresence('gender_id', true)
            ->notEmptyString('gender_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('please fill out this field'),false);

        $validator
            ->email('email')
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('agency')
            ->maxLength('agency', 255)
            ->requirePresence('agency', true)
            ->notEmptyString('agency', ucwords('please fill out this field'),false);

        $validator
            ->scalar('transaction_code')
            ->maxLength('transaction_code', 255)
            ->requirePresence('transaction_code', true)
            ->notEmptyString('transaction_code', ucwords('please fill out this field'),false);

        $validator
            ->numeric('is_answered')
            ->requirePresence('is_answered', true)
            ->notEmptyString('is_answered', ucwords('please fill out this field'),false)
            ->add('is_answered','is_answered',[
                'rule' => function($value){
                    if(!in_array(intval($value),[0, 1])){
                        return ucwords('Visitor have not answered any surveys');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_visited')
            ->requirePresence('is_visited', true)
            ->notEmptyString('is_visited', ucwords('please fill out this field'),false)
            ->add('is_visited','is_visited',[
                'rule' => function($value){
                    if(!in_array(intval($value),[0, 1])){
                        return ucwords('Visitor must check the visitor visited');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('is_cloned')
            ->requirePresence('is_cloned', true)
            ->notEmptyString('is_cloned', ucwords('please fill out this field'),false)
            ->add('is_cloned','is_cloned',[
                'rule' => function($value){
                    if(!in_array(intval($value),[0, 1])){
                        return ucwords('Visitor must check clone value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['gender_id'], 'Genders'), ['errorField' => 'gender_id']);
        $rules->add($rules->existsIn(['span_id'], 'Spans'), ['errorField' => 'span_id']);
        $rules->add($rules->existsIn(['office_id'], 'Offices'), ['errorField' => 'office_id']);
        $rules->add($rules->existsIn(['department_id'], 'Departments'), ['errorField' => 'department_id']);

        return $rules;
    }

    public function transactioncode(){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $transaction_code = $this->find()
            ->where([
                'created >=' => $startMonth,
                'created <=' => $endMonth,
            ])->count();
        $date = date('Y').date('m').date('d-');
        $transaction_code = $date.str_pad(strval($transaction_code+1), 1, '0', STR_PAD_LEFT);
        return $transaction_code;
    }

}
