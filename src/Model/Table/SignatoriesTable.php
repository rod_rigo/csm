<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Signatories Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Signatory newEmptyEntity()
 * @method \App\Model\Entity\Signatory newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Signatory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Signatory get($primaryKey, $options = [])
 * @method \App\Model\Entity\Signatory findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Signatory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Signatory[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Signatory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Signatory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SignatoriesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('signatories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){
            $connection->begin();
            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        if($entity->isDirty() && intval($i) == intval(0)){
            $connection->begin();
            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');
        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('signatory')
            ->maxLength('signatory', 255)
            ->requirePresence('signatory', true)
            ->notEmptyString('signatory', ucwords('please fill out this field'), false);

        $validator
            ->scalar('position')
            ->maxLength('position', 255)
            ->requirePresence('position', true)
            ->notEmptyString('position', ucwords('please fill out this field'), false);

        $validator
            ->numeric('order_position')
            ->requirePresence('order_position', true)
            ->notEmptyString('order_position', ucwords('please fill out this field'), false);

        $validator
            ->notEmptyString('is_active')
            ->requirePresence('is_active', true)
            ->notEmptyString('is_active', ucwords('Please fill out this field'), false)
            ->add('is_active', 'is_active',[
                'rule' => function($value){
                    $isActive = [0, 1];
                    if(!in_array($value, $isActive)){
                        return ucwords('please select active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['signatory'],ucwords('this signatory is already exists')), ['errorField' => 'signatory']);


        return $rules;
    }
}
