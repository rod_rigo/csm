<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Name Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $age
 * @property int $gender_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 */
class Name extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'age' => true,
        'gender_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
    ];

    protected function _setName($value){
        return strtoupper($value);
    }

}
