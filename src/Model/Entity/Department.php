<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Department Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $office_id
 * @property string $department
 * @property string $legend
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\Service[] $services
 * @property \App\Model\Entity\Survey[] $surveys
 * @property \App\Model\Entity\Profile[] $profiles
 * @property \App\Model\Entity\Visitor[] $visitors
 *
 */
class Department extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'office_id' => true,
        'department' => true,
        'legend' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'office' => true,
        'services' => true,
        'surveys' => true,
        'profiles' => true,
        'visitors' => true,
    ];

    protected function _setDepartment($value){
        return ucwords($value);
    }

    protected function _setLegend($value){
        return strtoupper($value);
    }

}
