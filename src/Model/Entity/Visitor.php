<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Visitor Entity
 *
 * @property int $id
 * @property int $logical_id
 * @property int $assist_id
 * @property string $name
 * @property string $email
 * @property int $age
 * @property int $gender_id
 * @property int $office_id
 * @property int $department_id
 * @property int $span_id
 * @property string $agency
 * @property string $transaction_code
 * @property int $is_answered
 * @property int $is_visited
 * @property int $is_cloned
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\Survey[] $surveys
 * @property \App\Model\Entity\Span $span
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\Department $department
 */
class Visitor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'logical_id' => true,
        'assist_id' => true,
        'name' => true,
        'email' => true,
        'age' => true,
        'gender_id' => true,
        'span_id' => true,
        'office_id' => true,
        'department_id' => true,
        'agency' => true,
        'transaction_code' => true,
        'is_answered' => true,
        'is_visited' => true,
        'is_cloned' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'gender' => true,
        'surveys' => true,
        'span' => true,
        'office' => true,
        'department' => true,
    ];

    protected function _setName($value){
        return strtoupper($value);
    }

    protected function _setAgency($value){
        return strtoupper($value);
    }

}
